package de.hatur.maygame.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import de.hatur.maygame.MayGame;
import de.hatur.maygame.external.IActivityRequestHandler;

public class DesktopLauncher implements IActivityRequestHandler {
	// For AdView / interface because of the static context
	private static DesktopLauncher application;
	public static void main (String[] arg) {
		if (application == null) {
			application = new DesktopLauncher();
		}
		
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 800;
		config.height = 480;
		config.vSyncEnabled = true;
		config.resizable = false;
		config.title = MayGame.GAME_NAME;
		new LwjglApplication(new MayGame(application), config);
	}

	@Override
	public void showAds(boolean show) {
	}
}

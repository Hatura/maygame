package de.hatur.maygame.particle;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.hatur.maygame.MayGame;

public class Particle {	
	protected MayGame game;
	protected Group actorGroup;
	
	public enum StandardType {
		TOP_BOTTOM,
		BOTTOM_TOP,
		RIGHT_LEFT,
		LEFT_RIGHT
	}
	
	public enum TransformationType {
		RANDOM_ROTATION;
	}
	
	protected StandardType standardType;
	protected TransformationType transformationType;
	protected Interpolation interpolation;
	
	protected int PARTICLE_WIDTH;
	protected int PARTICLE_HEIGHT;
	protected float PARTICLE_DURATION;
	
	protected Image particle;

	protected int startX;
	protected int startY;
	
	protected int targetX;
	protected int targetY;
	
	public Particle(Group actorGroup, Image particleImage, float fDuration) {
		this.actorGroup = actorGroup;
		this.PARTICLE_DURATION = fDuration;
		setParticleImage(particleImage);
		init();
	}
	
	public Particle(Group actorGroup, Image particleImage, float fDuration, StandardType standardType, TransformationType transformationType, Interpolation interpolation) {
		this.actorGroup = actorGroup;
		this.PARTICLE_DURATION = fDuration;
		this.standardType = standardType;
		this.transformationType = transformationType;
		this.interpolation = interpolation;
		setParticleImage(particleImage);
		init();
	}
	
	protected void init() {
		setStartCoords();
		setTransformation();
		setTargetCoords();
		setAction();
	}
	
	/**
	 * Needs to be called! Sets the particle image
	 */
	private void setParticleImage (Image image) {
		this.particle = image;
		
		PARTICLE_WIDTH = (int) particle.getWidth();
		PARTICLE_HEIGHT = (int) particle.getHeight();
	}
	
	/**
	 * Sets the starting coordinates of the particle
	 */
	protected void setStartCoords() {
		if (standardType != null) {
			switch (standardType) {
			case TOP_BOTTOM:
				startX = (int) (Math.random() * (MayGame.VIRTUAL_WIDTH - PARTICLE_WIDTH));
				startY = MayGame.VIRTUAL_HEIGHT + PARTICLE_HEIGHT;
				break;
			case BOTTOM_TOP:
				break;
			case LEFT_RIGHT:
				startX = 0 - PARTICLE_WIDTH;
				startY = (int) (Math.random() * MayGame.VIRTUAL_HEIGHT - PARTICLE_HEIGHT);
				break;
			case RIGHT_LEFT:
				startX = MayGame.VIRTUAL_WIDTH + PARTICLE_WIDTH;
				startY = (int) (Math.random() * MayGame.VIRTUAL_HEIGHT - PARTICLE_HEIGHT);
				break;
			default:
				break;
			}
		}
	}
	
	/**
	 * Add transformation to particle .. scaling, rotation etc.
	 */
	protected void setTransformation() {
		if (transformationType != null) {
			switch (transformationType) {
			case RANDOM_ROTATION:
				particle.rotateBy((float)(Math.random() * 360));
				break;
			default:
				System.out.println("Wrong transformation type");
				break;
			}
		}
	}
	
	/**
	 * Set destination coordinates of the parrticle
	 */
	protected void setTargetCoords() {
		if (standardType != null) {
			switch (standardType) {
			case TOP_BOTTOM:
				targetX = startX;
				targetY = 0 - PARTICLE_HEIGHT;
				break;
			case BOTTOM_TOP:
				break;
			case LEFT_RIGHT:
				targetX = MayGame.VIRTUAL_WIDTH + PARTICLE_WIDTH;
				targetY = startY;
				break;
			case RIGHT_LEFT:
				targetX = 0 - PARTICLE_WIDTH;
				targetY = startY;
				break;
			default:
				break;
			}
		}
	}
	
	protected void setAction() {
		particle.setTouchable(Touchable.disabled);
		particle.setPosition(startX, startY);
		
		MoveToAction mtAction = new MoveToAction();
		mtAction.setDuration(PARTICLE_DURATION);
		mtAction.setPosition(targetX, targetY);
		if (interpolation != null)
			mtAction.setInterpolation(interpolation);
		
		particle.addAction(mtAction);
	}
	
	/**
	 * Add particle to stage with predefinied libgdx interpolation
	 * @param interpolation
	 */
	protected void addToGroup() {
		actorGroup.addActor(particle);
	}
}

package de.hatur.maygame.particle;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import de.hatur.maygame.particle.Particle.StandardType;
import de.hatur.maygame.particle.Particle.TransformationType;

public class ParticleManager {
	
	private Group actorGroup;
	private float fTimeCounter;
	private long lastClearTime;
	private final int clearInterval = 5000;
	private boolean bIsStarted;
	
	public ParticleManager(Group actorGroup) {
		this.actorGroup = actorGroup;
		fTimeCounter = 0;
		lastClearTime = 0;
		bIsStarted = false;
	}
	
	/**
	 * Adds, relative to deltatime, images (particles) to the predefined backgroundGroup
	 * @param delta = delta time
	 * @param fWaitTime = time between adds
	 * @param imageDrawable = particleimage
	 * @param fTravelDuration = length of travel
	 * @param standardType = top to bottom, bottom to top, etc.
	 * @param interpolation = predefined libgdx Interpolation
	 */
	public void manage(float delta, float fWaitTime, Drawable imageDrawable, float fTravelDuration, StandardType standardType, 
			TransformationType transformationType, Interpolation interpolation) {
		fTimeCounter += delta;
		int iCount = 0;
		while (fTimeCounter > fWaitTime) {
			if (iCount > 20) { // limit off to prevent overflow
				this.fTimeCounter = fWaitTime;
				break;
			}
			new Particle(actorGroup, new Image(imageDrawable), fTravelDuration, standardType, transformationType, interpolation).addToGroup();
			fTimeCounter -= fWaitTime;
			iCount++;
		}
		
		if (System.currentTimeMillis() > lastClearTime + clearInterval) {
			cleanUp();
			lastClearTime = System.currentTimeMillis();
		}
	}
	
	public void start() {
		bIsStarted = true;
	}
	
	public boolean isStarted() {
		return bIsStarted;
	}
	
	/**
	 * Clears the group of Actors from action-finished Actors
	 */
	private void cleanUp() {
		for (int i = 0; i < actorGroup.getChildren().size; i++) {
			if (actorGroup.getChildren().get(i).getActions().size == 0) {
				actorGroup.getChildren().removeIndex(i);
				actorGroup.getChildren().shrink();
			}
		}
	}
	
	public void dispose() {
		actorGroup.getChildren().clear();
	}
}

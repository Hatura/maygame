package de.hatur.maygame.handles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.Disposable;

public class ShaderManager implements Disposable {
	
	public enum Shader {
		DEFAULT,
		INVERT,
		PASSTHROUGH,
		BLUR
	}
	
	private ShaderProgram shaderProgram;
	private Shader currentShader;
	
	// DEFAULT
	private final String DEFAULT_VERT = "data/shader/default_vert.glsl";
	private final String DEFAULT_FRAG = "data/shader/default_frag.glsl";
	private float default_sepia_Intensity = 0.0f;
	private boolean default_isHurtFlashApplied = false;
	private float default_hurtFlash_Intensity = 0.0f;
	
	// INVERT
	private final String INVERT_VERT = "data/shader/invert_vert.glsl";
	private final String INVERT_FRAG = "data/shader/invert_frag.glsl";
	
	// PASSTHROUGH
	private final String PASSTHROUGH_VERT = "data/shader/passthrough_vert.glsl";
	private final String PASSTHROUGH_FRAG = "data/shader/passthrough_frag.glsl";
	
	// BLUR
	private final String BLUR_VERT = null;
	private final String BLUR_FRAG = null;
	
	/**
	 * using default shader
	 */
	public ShaderManager() {
		currentShader = Shader.DEFAULT;
		initShaderManager();
	}
	
	/**
	 * start with predefined shader
	 * @param startShader enum Shader
	 */
	public ShaderManager(Shader startShader) {
		currentShader = startShader;
		initShaderManager();
	}
	
	/**
	 * used private to init shaderstuff - to not do it in every different constructor
	 */
	private void initShaderManager() {
		ShaderProgram.pedantic = false;
		shaderProgram = new ShaderProgram(Gdx.files.internal(getCurrentVertShader()), Gdx.files.internal(getCurrentFragShader()));
		System.out.println(shaderProgram.isCompiled() ? "Shader successfully compiled" : shaderProgram.getLog());
		setStartUniforms();
	}
	
	@Override
	public String toString() {
		return "ShaderManager [currentShader=" + currentShader + "]";
	}
	
	/**
	 * Get the current defined VertexShader FileHandle
	 * @return the current defined VertexShader FileHandle
	 */
	private String getCurrentVertShader() {
		switch (currentShader) {
		case DEFAULT:
			return DEFAULT_VERT;
		case INVERT:
			return INVERT_VERT;
		case PASSTHROUGH:
			return PASSTHROUGH_VERT;
		case BLUR:
			return BLUR_VERT;
		default:
			return null;
		}
	}
	
	/**
	 * Get the current defined FragmentShader FileHandle
	 * @return the current defined FragmentShader FileHandle
	 */
	private String getCurrentFragShader() {
		switch (currentShader) {
		case DEFAULT:
			return DEFAULT_FRAG;
		case INVERT:
			return INVERT_FRAG;
		case PASSTHROUGH:
			return PASSTHROUGH_FRAG;
		case BLUR:
			return BLUR_FRAG;
		default:
			return null;
		}
	}
	
	/**
	 * Pass start values
	 */
	private void setStartUniforms() {
		switch (currentShader) {
		default:
			break;
		}
	}
	
	/**
	 * Update shader effects according to current selected shader type
	 */
	public void updateShaderEffects(float delta) {
		shaderProgram.begin();
		
		switch (currentShader) {
		case DEFAULT:
			shaderProgram.setUniformf("u_resolution", Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
			shaderProgram.setUniformf("u_sepiaintensity", default_sepia_Intensity);
			shaderProgram.setUniformf("u_flashintensity", default_hurtFlash_Intensity);
			
			if (default_isHurtFlashApplied)
				default_updateHurtFlash(delta);
			break;
		default:
			break;
		}
		
		shaderProgram.end();
	}
	
	/**
	 * Set shader to new shader
	 * @param newShader enum Shader
	 */
	public void setShader(Shader newShader) {
		currentShader = newShader;
		shaderProgram.dispose();
		initShaderManager();
	}
	
	/**
	 * return shaderProgram to use for extern spritebatches
	 * @return
	 */
	public ShaderProgram getShaderProgram() {
		return shaderProgram;
	}
	
	/**
	 * To get the current sepia intensity, 0.0 = no sepia - 1.0 = full sepia
	 * @return
	 */
	public float default_setSepiaIntensity() {
		return default_sepia_Intensity;
	}
	
	/**
	 * To set the screen to sepia colors using the sepia shader, 0.0 = no sepia - 1.0 = full sepia
	 * @param intensity
	 */
	public void default_getSepiaIntensity(float intensity) {
		if (intensity < 0)
			intensity = 0;
		else if (intensity > 1f)
			intensity = 1f;
		
		default_sepia_Intensity = intensity;
	}
	
	public void default_applyHurtFlash() {
		default_hurtFlash_Intensity = 0.4f;
		default_isHurtFlashApplied = true;
	}
	
	public void default_updateHurtFlash(float delta) {
		default_hurtFlash_Intensity -= delta * 3;
		
		if (default_hurtFlash_Intensity < 0) {
			default_isHurtFlashApplied = false;
			default_hurtFlash_Intensity = 0;
		}
	}
	
	/**
	 * dispose shaderProgram
	 */
	public void dispose() {
		shaderProgram.dispose();
	}
}

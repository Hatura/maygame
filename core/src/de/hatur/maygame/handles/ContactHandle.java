package de.hatur.maygame.handles;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

import de.hatur.maygame.MayGame;
import de.hatur.maygame.handles.DogeEmote.EmoteType;
import de.hatur.maygame.handles.Jetpack.Direction;
import de.hatur.maygame.handles.LevelManager.DeathReason;
import de.hatur.maygame.handles.SoundManager.SoundType;
import de.hatur.maygame.screens.GameScreen;

public class ContactHandle implements ContactListener {
	
	private MayGame game;
	private GameScreen gameScreen;
	
	private DogeData da = null;
	private DogeData db = null;
	private boolean bCalculateImpactForce;
	private boolean bForceCalculationDone;
	
	private final double PLAYER_MIN_DMG = 25;
	private final double PLAYER_MIN_DMG_TRIGGER = 33.3;
	
	private final double JETPACK_MIN_DMG = 25;
	private final double JETPACK_MIN_DMG_TRIGGER = 33.3;
	
	public ContactHandle (MayGame game, GameScreen gameScreen) {
		this.game = game;
		this.gameScreen = gameScreen;
	}

	@Override
	public void beginContact(Contact contact) {
		Fixture fa = contact.getFixtureA();
		Fixture fb = contact.getFixtureB();
		
		bCalculateImpactForce = false;
		bForceCalculationDone = false;
		
		da = (DogeData) fa.getBody().getUserData();
		db = (DogeData) fb.getBody().getUserData();
		
//		if (isAffected("environment")) // dont check anythiung colliding with just static environment
//			return;
		if (isAffected("mdogecoin") || isAffected("mbitcoin") || isAffected("mlitecoin")) {
			if (!gameScreen.levelManager.isLevelStarted())
				return;
			
			// both are coins
			if (da.isMovableCoin() && db.isMovableCoin()) {
				if (db.isActive())
					da.setCollided(true);
				if (da.isActive())
					db.setCollided(true);
			}
			
			else {
				String sTypeName = "";
				if (isAffected("mdogecoin"))
					sTypeName = "mdogecoin";
				else if (isAffected("mlitecoin"))
					sTypeName = "mlitecoin";
				else if (isAffected("mbitcoin"))
					sTypeName = "mbitcoin";
				
				DogeData affectedDoge = getDogeInstance(sTypeName);
				
				// set only if collided with solid block
				if (checkSolidCollision())
					affectedDoge.setCollided(true);
			}
		}
		
		if ((checkCollision("dogecoin", "player") || checkCollision("dogecoin", "jetpack")
				|| checkCollision("mdogecoin", "player") || checkCollision("mdogecoin", "jetpack")) && !gameScreen.levelManager.isLevelFinished()) {
			DogeData affectedDoge = null;
			if (isAffected("dogecoin"))
				affectedDoge = getDogeInstance("dogecoin");
			else
				affectedDoge = getDogeInstance("mdogecoin");
			
			if (affectedDoge.isActive()) {
				if (gameScreen.player.isAlive()) {
					gameScreen.levelManager.collectDogecoin();
					game.soundManager.playSound(SoundType.COIN1);
					affectedDoge.disableObject();
				}
			}
		}
		else if ((checkCollision("bitcoin", "player") || checkCollision("bitcoin", "jetpack")
				|| checkCollision("mbitcoin", "player") || checkCollision("mbitcoin", "jetpack")) && !gameScreen.levelManager.isLevelFinished()) {
			DogeData affectedDoge = null;
			if (isAffected("bitcoin"))
				affectedDoge = getDogeInstance("bitcoin");
			else
				affectedDoge = getDogeInstance("mbitcoin");
			
			if (affectedDoge.isActive()) {
				if (gameScreen.player.isAlive()) {
					gameScreen.levelManager.collectBitcoin();
					game.soundManager.playSound(SoundType.COIN2);
					affectedDoge.disableObject();
				}
			}
		}
		else if ((checkCollision("litecoin", "player") || checkCollision("litecoin", "player")
				|| checkCollision("mlitecoin", "player") || checkCollision("mlitecoin", "jetpack")) && !gameScreen.levelManager.isLevelFinished()) {
			DogeData affectedDoge = null;
			if (isAffected("litecoin"))
				affectedDoge = getDogeInstance("litecoin");
			else
				affectedDoge = getDogeInstance("mlitecoin");
			
			if (affectedDoge.isActive()) {
				if (gameScreen.player.isAlive()) {
					gameScreen.levelManager.collectLitecoin();
					game.soundManager.playSound(SoundType.COIN3);
					affectedDoge.disableObject();
				}
			}
		}
		else if (checkCollision("sbtop", "player"))
			gameScreen.getJetpack().applySpeedBoost(Direction.TOP);
		else if (checkCollision("sbbottom", "player"))
			gameScreen.getJetpack().applySpeedBoost(Direction.BOTTOM);
		else if (checkCollision("sbleft", "player"))
			gameScreen.getJetpack().applySpeedBoost(Direction.LEFT);
		else if (checkCollision("sbright", "player"))
			gameScreen.getJetpack().applySpeedBoost(Direction.RIGHT);
		else if (checkCollision("jerrycan", "player")) {
			DogeData affectedDoge = getDogeInstance("jerrycan");
			if (affectedDoge.isActive()) {
				gameScreen.getJetpack().refuel(20f);
				game.soundManager.playSound(SoundType.REFUEL);
				affectedDoge.disableObject();
			}			
		}
		else if (checkCollision("groundcat", "player")) {
			game.soundManager.playSound(SoundType.HIT);
			bCalculateImpactForce = true;
		}
		else if (checkCollision("flycat", "player")) {
			game.soundManager.playSound(SoundType.HIT);
			bCalculateImpactForce = true;
		}
		else if (checkCollision("dolan", "player")) {
			game.soundManager.playSound(SoundType.HIT);
			bCalculateImpactForce = true;
		}
		else if (checkCollision("saw", "player")) {
			game.soundManager.playSound(SoundType.SAWHIT);
			if (game.optionsManager.isGraphicsHigh())
				gameScreen.getWorldShaderManager().default_applyHurtFlash();
			gameScreen.getPlayer().applyDamage(MathUtils.random(45f, 55f)); // fixed amount
		}
		else if (checkCollision("groundcat", "jetpack")) {
			Boolean isIntact = (Boolean) gameScreen.getPlayerJoint().getUserData();
			if (isIntact) {
				game.soundManager.playSound(SoundType.HIT);
				bCalculateImpactForce = true;
			}
		}
		else if (checkCollision("flycat", "jetpack")) {
			Boolean isIntact = (Boolean) gameScreen.getPlayerJoint().getUserData();
			if (isIntact) {
				game.soundManager.playSound(SoundType.HIT);
				bCalculateImpactForce = true;
			}
		}
		else if (checkCollision("dolan", "jetpack")) {
			Boolean isIntact = (Boolean) gameScreen.getPlayerJoint().getUserData();
			if (isIntact) {
				game.soundManager.playSound(SoundType.HIT);
				bCalculateImpactForce = true;
			}
		}
		else if (checkCollision("saw", "jetpack")) {
			Boolean isIntact = (Boolean) gameScreen.getPlayerJoint().getUserData();
			if (isIntact) {
				game.soundManager.playSound(SoundType.SAWHIT);
				if (game.optionsManager.isGraphicsHigh())
					gameScreen.getWorldShaderManager().default_applyHurtFlash();
				gameScreen.getJetpack().applyDamage(MathUtils.random(45f, 55f)); // fixed amount
			}
		}
		else if (checkCollision("almostdonearea", "player")) {
			gameScreen.dogeEmote.newEmote(EmoteType.ALMOST_DONE);
		}
		else if (checkCollision("lossarea", "player")) {
//			game.soundManager.playSound(SoundType.DESTRUCTION);
			gameScreen.dogeEmote.newEmote(EmoteType.DEFEAT);
			gameScreen.levelManager.setLevelFinished(false, DeathReason.DESERTION);
		}
		else if (checkCollision("winarea", "player")) {
			gameScreen.levelManager.setLevelFinished(true, null);
		}

	}

	@Override
	public void endContact(Contact contact) {
		
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		if (bCalculateImpactForce && !bForceCalculationDone) {
			// only use shader if graphics high
			if (game.optionsManager.isGraphicsHigh())
				gameScreen.getWorldShaderManager().default_applyHurtFlash();
			if (isAffected("jetpack"))
				calculateJetpackDamage(impulse);
			else if (isAffected("player"))
				calculatePlayerDamage(impulse);
			else
				System.out.println("Told to calculate impact force, but no legit object affected! object 1: " + da.getType() + ", object 2: " + db.getType());
		}
	}
	
	private boolean checkCollision (String sObj1, String sObj2) {
		if ((da.getType().equals(sObj1) && db.getType().equals(sObj2)) || (da.getType().equals(sObj2) && db.getType().equals(sObj1)))
			return true;
		else
			return false;
	}
	
	private boolean checkSolidCollision () {
		return da.isSolid() || db.isSolid() ? true : false;
	}
	
	private DogeData getDogeInstance (String sObjectname) {
		return da.getType().equals(sObjectname) ? da : db;
	}
	
	private boolean isAffected(String sObj) {
		if (da.getType().equals(sObj) || db.getType().equals(sObj))
			return true;
		return false;
	}
	
	private void calculateJetpackDamage(ContactImpulse impulse) {
//		System.out.println("Forces between " + da.getType() + ", " + db.getType() + ":");
		
		float fForce = Math.abs(impulse.getNormalImpulses()[0]);
		
//		System.out.println("Force: " + fForce);
		
		// Correct Force
		fForce = fForce * 9; // to match low density
		
//		System.out.println(Math.log(fForce+1));
//		System.out.println("Amount: " + fForce);
		
		if (fForce < JETPACK_MIN_DMG_TRIGGER) {
			fForce = (float) JETPACK_MIN_DMG + (float)(Math.random() * ((JETPACK_MIN_DMG_TRIGGER - JETPACK_MIN_DMG) + 1));
		}
		
//		System.out.println("New Amount: " + fForce);
		
		gameScreen.getJetpack().applyDamage(fForce);
		
		bForceCalculationDone = true;
	}
	
	private void calculatePlayerDamage(ContactImpulse impulse) {
//		System.out.println("Forces between " + da.getType() + ", " + db.getType() + ":");
		
		float fForce = Math.abs(impulse.getNormalImpulses()[0]);
		
//		System.out.println("Force: " + fForce);
		
		// Correct Force
		fForce = fForce * 7;

//		System.out.println(Math.log(fForce+1));
//		System.out.println("Amount: " + fForce);
		
		if (fForce < PLAYER_MIN_DMG_TRIGGER) {
			fForce = (float) PLAYER_MIN_DMG + (float)(Math.random() * ((PLAYER_MIN_DMG_TRIGGER - PLAYER_MIN_DMG) + 1));
		}
		
//		System.out.println("New Amount: " + fForce);
		
		gameScreen.player.applyDamage(fForce);
		
		bForceCalculationDone = true;
	}

}

package de.hatur.maygame.handles;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

import de.hatur.maygame.MayGame;
import de.hatur.maygame.entity.Entity;
import de.hatur.maygame.handles.DogeEmote.EmoteType;
import de.hatur.maygame.handles.LevelManager.DeathReason;
import de.hatur.maygame.handles.SoundManager.SoundType;
import de.hatur.maygame.screens.GameScreen;

public class Player implements Entity {
	private MayGame game;
	private GameScreen gameScreen;
	private Body body;
	
	private Vector2 lastPosition;
	private float lastAngle;
	
	private float fLife;
	private boolean bDefeatSoundPlayed = false;
	
	public Player(MayGame game, GameScreen gameScreen, Body body) {
		this.game = game;
		this.gameScreen = gameScreen;
		this.body = body;
		
		fLife = 100.0f;
		
		Entity.minionList.add(this);
	}
	
	public float getLife() {
		return (float) Math.ceil(Math.max(0, (Math.round(fLife * 100.0) / 100.0)));
	}
	
	public boolean isAlive () {
		return fLife > 0.0f ? true : false;
	}
	
	public void applyDamage(float fAmount) {
		fLife -= fAmount;
		
		if (fLife <= 0 && !bDefeatSoundPlayed) {
			game.soundManager.playSound(SoundType.DOGEDEFEATED);
			bDefeatSoundPlayed = true;
			gameScreen.levelManager.setLevelFinished(false, DeathReason.DOGEDEAD);
		}
	}
	
	public Body getPlayerBody() {
		return body;
	}

	@Override
	public void update() {
		// Limit player speed
		// Y-Speed
		if (body.getLinearVelocity().y > Jetpack.MAX_SPEED_Y) {
			body.applyForceToCenter(0, -(Jetpack.JETPACK_Y_ACCELERATION), true);
			gameScreen.dogeEmote.newEmote(EmoteType.YSPEED);
		}
		// X-Speed
		if (body.getLinearVelocity().x > Jetpack.MAX_SPEED_X) {
			body.applyForceToCenter(-(Jetpack.JETPACK_X_ACCELERATION), 0, true);
			gameScreen.dogeEmote.newEmote(EmoteType.XSPEED);
		}
		else if (body.getLinearVelocity().x < -Jetpack.MAX_SPEED_X) {
			body.applyForceToCenter((Jetpack.JETPACK_X_ACCELERATION), 0, true);
			gameScreen.dogeEmote.newEmote(EmoteType.XSPEED);
		}
		
		// Emote for angular Speed
		if ((body.getAngularVelocity() > 6f || body.getAngularVelocity() < -6f) && (body.getAngle() > 2.0f || body.getAngle() < -2.0f))
			gameScreen.dogeEmote.newEmote(EmoteType.ANGLE);
	}

	@Override
	public void victoryEmote(SpriteBatch batch, BitmapFont font) {
	}

	@Override
	public void saveLastPosition() {
		lastPosition = body.getPosition();
		lastAngle = body.getAngle();
	}
}

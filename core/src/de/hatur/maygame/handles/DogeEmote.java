package de.hatur.maygame.handles;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class DogeEmote {
	public enum EmoteType {
		ANGLE,
		YSPEED,
		XSPEED,
		COLLISION,
		ALMOST_DONE,
		DEFEAT
	};
	
	private SpriteBatch batch;
	private BitmapFont font;
	private EmoteType emoteType;
	private Random randomGenerator;
	private long emoteStartTime;
	private final int iEmoteStayTime = 2500; // in ms
	private String sEmote;
	
	private ArrayList<String> emoteAngle;
	private ArrayList<String> emoteCollision;
	private ArrayList<String> emoteYSpeed;
	private ArrayList<String> emoteXSpeed;
	private ArrayList<String> emoteAlmostDone;
	private ArrayList<String> emoteDefeat;
	
	public DogeEmote(SpriteBatch batch, BitmapFont font) {
		this.batch = batch;
		this.font = font;
		emoteStartTime = 0;
		randomGenerator = new Random();
		loadEmoteTexts();
	}
	
	private void loadEmoteTexts() {
		emoteAngle = new ArrayList<String>();
		emoteCollision = new ArrayList<String>();
		emoteYSpeed = new ArrayList<String>();
		emoteXSpeed = new ArrayList<String>();
		emoteAlmostDone = new ArrayList<String>();
		emoteDefeat = new ArrayList<String>();
		
		emoteAngle.add("Wow! Many sick!");
		emoteAngle.add("WOW!");
		emoteAngle.add("So sick");
		
		emoteYSpeed.add("So speed");
		emoteYSpeed.add("Much confidence!");
		emoteYSpeed.add("So hyperspeed!");
		emoteYSpeed.add("Many fast");
		emoteYSpeed.add("incoming");
		
		emoteXSpeed.add("Going to NASCAR soon");
		emoteXSpeed.add("Wow, fast!");
		
		emoteCollision.add("Wow! Much pain!");
		emoteCollision.add("Cat hurt");
		
		emoteAlmostDone.add("Almost there");
		emoteAlmostDone.add("Can see the end");
		emoteAlmostDone.add("Very close");
		
		emoteDefeat.add("No moon today");
		emoteDefeat.add("Cats win this time");
		emoteDefeat.add("Am hurt");
		emoteDefeat.add("Am going back home");
		emoteDefeat.add("So hurt");
	}
	
	public void newEmote(EmoteType emoteType) {
		if (System.currentTimeMillis() > emoteStartTime + iEmoteStayTime && emoteType != this.emoteType) {
			this.emoteType = emoteType;
			emoteStartTime = System.currentTimeMillis();
			sEmote = createEmote();
		}
	}
	
	public boolean isActive() {
		if (System.currentTimeMillis() < emoteStartTime + iEmoteStayTime && emoteType != null)
			return true;
		return false;
	}
	
	public void drawEmote(Vector2 playerPosition) {
		font.setColor(Color.WHITE);
		font.draw(batch, sEmote, playerPosition.x + 0.5f, playerPosition.y + 1.5f);
	}
	
	private String createEmote() {
		int iRandomNumber = -1;
		
		switch (emoteType) {
		case ANGLE:
			iRandomNumber = randomGenerator.nextInt(emoteAngle.size());
			return emoteAngle.get(iRandomNumber);
		case COLLISION:
			iRandomNumber = randomGenerator.nextInt(emoteCollision.size());
			return emoteCollision.get(iRandomNumber);
		case YSPEED:
			iRandomNumber = randomGenerator.nextInt(emoteYSpeed.size());
			return emoteYSpeed.get(iRandomNumber);
		case ALMOST_DONE:
			iRandomNumber = randomGenerator.nextInt(emoteAlmostDone.size());
			return emoteAlmostDone.get(iRandomNumber);
		case DEFEAT:
			iRandomNumber = randomGenerator.nextInt(emoteDefeat.size());
			return emoteDefeat.get(iRandomNumber);
		default:
			System.out.println("No such emote!");
			return "";
		}
	}

}

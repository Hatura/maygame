package de.hatur.maygame.handles;

import java.text.SimpleDateFormat;
import java.util.Date;

import de.hatur.maygame.MayGame;
import de.hatur.maygame.handles.DogeEmote.EmoteType;
import de.hatur.maygame.savemanager.SaveManager;
import de.hatur.maygame.screens.GameScreen;

public class LevelManager {
	private MayGame game;
	private GameScreen gameScreen;
	
	public enum DeathReason {
		DOGEDEAD,
		DESERTION, 
		NOFUEL,
		JETPACKDEAD
	};
	
	private int iLevel;
	private int iTotalDogecoin;
	private int iTotalBitcoin;
	private int iTotalLitecoin;
	
	private int iCollectedDogecoin;
	private int iCollectedBitcoin;
	private int iCollectedLitecoin;
	
	private boolean bIsMoonLevel = false;
	private boolean bIsLevelStarted;
	private boolean bIsLevelFinished;
	private boolean bIsResultVictory;
	private boolean bIsReadyforDefeatScreen;
	private long levelStartTime;
	private long levelFinishTime;
	
	/**
	 * For items and time, maybe rename to describe the class as more universal?
	 */
	public LevelManager(MayGame game, GameScreen gameScreen, int iLevel) {
		this.game = game;
		this.gameScreen = gameScreen;
		this.iLevel = iLevel;
	}
	
	/**
	 * sets the Items, needs to be called, inherited from game.saveManager !
	 * @param iTotalDogecoin
	 * @param iTotalBitcoin
	 * @param iTotalLitecoin
	 */
	public void setItems(int iTotalDogecoin, int iTotalBitcoin, int iTotalLitecoin) {
		this.iTotalDogecoin = iTotalDogecoin;
		this.iTotalBitcoin = iTotalBitcoin;
		this.iTotalLitecoin = iTotalLitecoin;
	}
	
	/**
	 * Sets the level to contain a moon, to show a different postgamevictoryscreen with the moon instead of stage finished
	 */
	public void setMoonLevel() {
		bIsMoonLevel = true;
	}
	
	/**
	 * is moon level
	 * @return if level is containing the moon
	 */
	public boolean isMoonlevel() {
		return bIsMoonLevel;
	}
	
	/**
	 * Sets begin level to true and takes time
	 */
	public void startLevel() {
		levelStartTime = System.currentTimeMillis();
		bIsLevelStarted = true;
		bIsLevelFinished = false;
		bIsResultVictory = false;
		bIsReadyforDefeatScreen = false;
		
		iCollectedDogecoin = 0;
		iCollectedBitcoin = 0;
		iCollectedLitecoin = 0;
	}
	
	/**
	 * 
	 * @return is Level started
	 */
	public boolean isLevelStarted() {
		return bIsLevelStarted;
	}
	
	/**
	 * For the real time TIMER
	 * @return passed time since Levelbegin 
	 */
	public String getTimePassed() {
		SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");
		Date passedTimeDate;
		if (bIsLevelStarted && !bIsLevelFinished) {
			passedTimeDate = new Date(System.currentTimeMillis() - levelStartTime);
			return sdf.format(passedTimeDate);
		}
		else if (bIsLevelFinished) {
			passedTimeDate = new Date(levelFinishTime - levelStartTime);
			return sdf.format(passedTimeDate);
		}
		else {
			passedTimeDate = new Date(0);
			return sdf.format(passedTimeDate);
		}
	}
	
	public String getTotalTime() {
		SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");
		Date passedTimeDate;
		passedTimeDate = new Date(levelFinishTime - levelStartTime);
		return sdf.format(passedTimeDate);
	}
	
	public int getCurrentLevel() {
		return iLevel;
	}
	
	public int getTotalDogecoin() {
		return iTotalDogecoin;
	}
	
	public int getTotalBitcoin() {
		return iTotalBitcoin;
	}
	
	public int getTotalLitecoin() {
		return iTotalLitecoin;
	}
	
	public int getCollectedDogecoin() {
		return iCollectedDogecoin;
	}
	
	public int getCollectedBitcoin() {
		return iCollectedBitcoin;
	}
	
	public int getCollectedLitecoin() {
		return iCollectedLitecoin;
	}
	
	/**
	 * collect one dogecoin
	 */
	public void collectDogecoin() {
		iCollectedDogecoin++;
	}
	
	/**
	 * collect one bitcoin
	 */
	public void collectBitcoin() {
		iCollectedBitcoin++;
	}
	
	/**
	 * collect one litecoin
	 */
	public void collectLitecoin() {
		iCollectedLitecoin++;
	}
	
	/**
	 * Set Level finished (the time)
	 */
	public void setLevelFinished(boolean bVictory, DeathReason reason) {
		bIsLevelFinished = true;
		levelFinishTime = System.currentTimeMillis();
		
		// Save data if victory
		if (bVictory)
			bIsResultVictory = true;
		
		if (reason != null)
			gameScreen.showDefeatPopup(reason);
		
		if (!bVictory) {
			gameScreen.dogeEmote.newEmote(EmoteType.DEFEAT);
			game.getMusicManager().setMusic_GameOver();
		}
	}
	
	/**
	 * level finished up by levelmanager?
	 * @return
	 */
	public boolean isLevelFinished() {
		return bIsLevelFinished;
	}
	
	/**
	 * called together with isLevelFinished() - determines if the result is victory or loss
	 */
	public boolean isResultVictory() {
		return bIsResultVictory;
	}
	
	/**
	 * to give extern access (needs to called at the end of render method, because world.step is causing crashed)
	 * @return
	 */
	public boolean isReadyForDefeatScreen() {
		return bIsReadyforDefeatScreen;
	}
	
	/**
	 * Flag that tells if ESC is pressed, then it switches to defeat screen
	 */
	public void setReadyForDefeatScreen() {
		bIsReadyforDefeatScreen = true;
	}
	
	public double getLevelPercentage() {
		double dPercentage = 
				(((double)iCollectedDogecoin + (2* (double)iCollectedBitcoin) + (2* (double)iCollectedLitecoin))
				* 100)
				/ ((double)getTotalDogecoin() + (2 * (double)getTotalBitcoin()) + (2 * (double)getTotalLitecoin()));
		
		return (Math.round(dPercentage * 100.0) / 100.0);
	}
	
	/**
	 * Calculates the percentage, score for the current Level, called in the off level screen
	 * @return boolean that tells if this is a new highscore
	 */
	public boolean calcAndSaveLevelData(SaveManager saveManager) {

		double dAchievedPercentage = getLevelPercentage();
		double dSavedPercentage = saveManager.levelRecords.get(iLevel).getPercentageDone(saveManager.levelData.get(iLevel));
		int iSavedTime = saveManager.levelRecords.get(iLevel).getTime();
		
		System.out.println("Current percentage: " + dAchievedPercentage + ", saved Percentage: " + saveManager.levelRecords.get(iLevel).getPercentageDone(saveManager.levelData.get(iLevel)));
		int iTime = (int) (levelFinishTime - levelStartTime);
		
		// Neuer Rekord
		if (dAchievedPercentage > dSavedPercentage || (dAchievedPercentage == dSavedPercentage && iTime < iSavedTime)) {			
			// Update Save data
			if (dAchievedPercentage > 80.0) // unlock next level at 80%
				saveManager.saveValue("level" + iLevel + "done", "true", false);
			saveManager.saveValue("level" + iLevel + "score", "1", false);
			saveManager.saveValue("level" + iLevel + "time", String.valueOf(iTime), false);
			saveManager.saveValue("level" + iLevel + "dogecoin", String.valueOf(iCollectedDogecoin), false);
			saveManager.saveValue("level" + iLevel + "bitcoin", String.valueOf(iCollectedBitcoin), false);
			saveManager.saveValue("level" + iLevel + "litecoin", String.valueOf(iCollectedLitecoin), true);
			
			// Update record state, for internal needs (no recalculation needed then)
			saveManager.levelRecords.get(iLevel).updateRecords(iTime, 1, iCollectedDogecoin, iCollectedBitcoin, iCollectedLitecoin);
			
			// Update unlocked of next record state if level is within range, for internal needs (no recalculation needed then)
			if (iLevel + 1 <= MayGame.AVAILABLE_LEVELS) {
				System.out.println("Unlocking next level");
				saveManager.levelRecords.get(iLevel + 1).setUnlocked();
			}
			return true;
		}
		else
			return false;
	}
	
}

package de.hatur.maygame.handles;

import java.util.ArrayList;

import javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.LifecycleListener;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;

import de.hatur.maygame.MayGame;

public class MusicManager implements Runnable, Disposable {
	private MayGame game;
	
	private enum ScreenState {
		MENU,
		GAME,
		GAMEOVER
	};
	
	public enum MusicType {
		FLAGS("data/audio/music/flagsm.ogg", 1.0f),
		ATLAST("data/audio/music/atlast.ogg", 0.05f),
		SADNESS("data/audio/music/sadness.ogg", 0.1f),
		SONG2("data/audio/crash.wav", 1.0f),
		SONG3("data/audio/coin3.wav", 1.0f);
		
		private String path;
		private float volume;
		
		MusicType(String path, float volume) {
			this.path = path;
			this.volume = volume;
		}
		
		public FileHandle getFile() {
			return Gdx.files.internal(path);
		}
		
		public float getVolume() {
			return volume;
		}
	};
	
	private ArrayList<MusicType> musicList = new ArrayList<MusicType>();
	
	private Music music;
	private ScreenState screenState;
	
	private boolean stopMusicRequest = false;
	private int lastSongThisList = -1;
	private float decreaseVolumeStep = -1.0f;
	private boolean forceMusicChange = false;
	
	/**
	 * Background Thread for music, checks if option is enabled, then what screen you are currently in and plays the music in a loop accordingly.
	 * This needs a major rework if there is more then one track in any requestmethod because of desync with the size of the arraylist
	 * @param game
	 */
	public MusicManager(MayGame game) {
		this.game = game;
	}

	@Override
	public void run() {
		while (!Thread.interrupted()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {}
			
			ArrayList<MusicType> musicSyncList = musicList;
			
			// Do not start music if the game is minimized / paused
			if (game.getDogeCycle().isGamePaused())
				continue;
			
			if (!game.optionsManager.isMusicEnabled() || stopMusicRequest) {
				stopAllMusic();
				stopMusicRequest = false;
				continue;
			}
			
			if (!forceMusicChange) {
				if (music != null && music.isPlaying()) {
					continue; // Do nothing, sleep again
				}
			}
			
			
			// Fade out
			if (music != null) {
				if (music.isPlaying() && music.getVolume() > 0) {
					if (decreaseVolumeStep == -1.0f)
						decreaseVolumeStep = music.getVolume();
					float newVolume = music.getVolume() - decreaseVolumeStep/10f;
					if (newVolume < 0)
						newVolume = 0;
					music.setVolume(newVolume);
					continue;
				}
			}
			
			// No song playing or fade out done
			loadNextTrack(musicSyncList);
		}
	}
	
	/**
	 * Gets the next track
	 */
	private void loadNextTrack(ArrayList<MusicType> musicSyncList) {
		try {
			if (!musicSyncList.isEmpty()) {
				int nextSong;
				if (lastSongThisList == -1) { // List just started
					nextSong = MathUtils.random(0, musicSyncList.size()-1);
				}
				else {
					if (lastSongThisList + 1 < musicSyncList.size()) {
						nextSong = lastSongThisList + 1;
					}
					else {
						nextSong = 0; // Wandered thorugh whole list
					}
				}
				
				lastSongThisList = nextSong;
				decreaseVolumeStep = -1.0f;
				
				// Stop last track if running
				if (music != null) {
					music.stop();
					music.dispose();
				}
				
				music = Gdx.audio.newMusic(musicSyncList.get(nextSong).getFile());
				
				music.play();
				music.setVolume(musicSyncList.get(nextSong).getVolume());
				forceMusicChange = false;
			}
		} catch (GdxRuntimeException e) {
			e.printStackTrace();
		}
	}
	
	public void setMusic_MenuDefault() {
		if (screenState == null || screenState != ScreenState.MENU) {
			musicList.clear();
			
			musicList.add(MusicType.FLAGS);
			
			lastSongThisList = -1;
			screenState = ScreenState.MENU;
			forceMusicChange = true;
		}
	}
	
	public void setMusic_GameDefault() {
		if (screenState == null || screenState != ScreenState.GAME) {
			musicList.clear();
			
			lastSongThisList = -1;
			screenState = ScreenState.GAME;
			forceMusicChange = true;
		}
	}
	
	/**
	 * Set specific tracks
	 * @param musicTypes
	 */
	public void setMusic_Game(MusicType... musicTypes) {
		if (screenState == null || screenState != ScreenState.GAME) {
			musicList.clear();
			
			for (MusicType music : musicTypes) {
				musicList.add(music);
			}
			
			lastSongThisList = -1;
			screenState = ScreenState.GAME;
			forceMusicChange = true;
		}
	}
	
	public void setMusic_GameOver() {
		if (screenState == null || screenState != ScreenState.GAMEOVER) {
			musicList.clear();
			
			musicList.add(MusicType.SADNESS);
			
			lastSongThisList = -1;
			screenState = ScreenState.GAMEOVER;
			forceMusicChange = true;
		}
	}
	
	private void stopAllMusic() {
		if (music != null) {
			music.stop();
			music.dispose();
			musicList.clear();
			lastSongThisList = -1;
			screenState = null;
		}
	}
	
	public void setStopMusicRequest() {
		stopMusicRequest = true;
	}

	@Override
	public void dispose() {
		if (music != null)
			music.dispose();
	}

}

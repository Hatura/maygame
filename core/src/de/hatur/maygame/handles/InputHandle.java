package de.hatur.maygame.handles;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;

import de.hatur.maygame.handles.ShaderManager.Shader;
import de.hatur.maygame.screens.GameScreen;

public class InputHandle implements InputProcessor {
	
	GameScreen gameScreen;
	
	public InputHandle(GameScreen gameScreen) {
		this.gameScreen = gameScreen;
	}

	@Override
	public boolean keyDown(int keycode) {
		switch (keycode) {
		case Keys.F2:
			if (gameScreen.getTableOverlay().isVisible())
				gameScreen.getTableOverlay().setVisible(false);
			else
				gameScreen.getTableOverlay().setVisible(true);
			break;
		case Keys.W:
			gameScreen.getCamera().position.y = gameScreen.getCamera().position.y + 25;
			gameScreen.getCamera().update();
			break;
		case Keys.S:
			gameScreen.getCamera().position.y = gameScreen.getCamera().position.y - 25;
			gameScreen.getCamera().update();
			break;
		case Keys.A:
			gameScreen.getCamera().position.x = gameScreen.getCamera().position.x - 25;
			gameScreen.getCamera().update();
			break;
		case Keys.D:
			gameScreen.getCamera().position.x = gameScreen.getCamera().position.x + 25;
			gameScreen.getCamera().update();
			break;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

}

package de.hatur.maygame.handles;

public class Countdown {
	private long startTime;
	private int iCountdownTime;
	
	/**
	 * Countdown in seconds, but internally measured in ms
	 * @param iCountdownTime
	 */
	public Countdown (int iCountdownTime) {
		this.iCountdownTime = iCountdownTime;
		this.startTime = System.currentTimeMillis();
	}
	
	public String getTime () {
		long tmpTime = (long) (Math.ceil(startTime/1000) + iCountdownTime - Math.ceil(System.currentTimeMillis()/1000));
		if (tmpTime <= 0)
			tmpTime = 0;
		return "" + tmpTime;
	}
	
	public boolean isFinished() {
		return System.currentTimeMillis() > startTime + (iCountdownTime*1000) ? true : false;
	}
}

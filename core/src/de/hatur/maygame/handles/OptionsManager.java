package de.hatur.maygame.handles;

import com.badlogic.gdx.graphics.Texture.TextureFilter;

import de.hatur.maygame.MayGame;

public class OptionsManager {
	private MayGame game;
	
	private boolean bSoundEnabled;
	private boolean bMusicEnabled;
	private TextureFilter textureFilter;
	
	public OptionsManager(MayGame game) {
		this.game = game;
		loadSavedOptions();
	}
	
	private void loadSavedOptions() {
		// Sound
		if (game.saveManager.isKeyExistant("soundenabled"))
			bSoundEnabled = game.saveManager.readBoolean("soundenabled");
		else {
			bSoundEnabled = true;
			game.saveManager.saveValue("soundenabled", "true", true);
		}
		
		// Music
		if (game.saveManager.isKeyExistant("musicenabled"))
			bMusicEnabled = game.saveManager.readBoolean("musicenabled");
		else {
			bMusicEnabled = true;
			game.saveManager.saveValue("musicenabled", "true", true);
		}
		
		// Texture Filtering
		if (game.saveManager.isKeyExistant("graphicsquality")) {
			String sQualityType = game.saveManager.readString("graphicsquality");
			if (sQualityType.equals("low"))
				textureFilter = TextureFilter.Nearest;
			else if (sQualityType.equals("high"))
				textureFilter = TextureFilter.Linear;
			else
				textureFilter = TextureFilter.Linear;
		}
		else {
			textureFilter = TextureFilter.Linear;
			game.saveManager.saveValue("graphicsquality", "high", true);
		}
	}
	
	public boolean isSoundEnabled() {
		return bSoundEnabled;
	}
	
	public boolean isMusicEnabled() {
		return bMusicEnabled;
	}
	
	public void toggleSoundEnabled() {
		bSoundEnabled = bSoundEnabled ? false : true;
		if (bSoundEnabled)
			game.saveManager.saveValue("soundenabled", "true", true);
		else
			game.saveManager.saveValue("soundenabled", "false", true);
	}
	
	public void toggleMusicEnabled() {
		bMusicEnabled = bMusicEnabled ? false : true;
		if (bMusicEnabled)
			game.saveManager.saveValue("musicenabled", "true", true);
		else
			game.saveManager.saveValue("musicenabled", "false", true);
	}
	public boolean isGraphicsHigh() {
		return textureFilter == TextureFilter.Linear ? true : false;
	}
	
	public TextureFilter getGraphicsParams() {
		return textureFilter;
	}
	
	public void toggleGraphics() {
		if (textureFilter == TextureFilter.Linear) {
			textureFilter = TextureFilter.Nearest;
			game.saveManager.saveValue("graphicsquality", "low", true);
		}
		else {
			textureFilter = TextureFilter.Linear;
			game.saveManager.saveValue("graphicsquality", "high", true);
		}
	}
}

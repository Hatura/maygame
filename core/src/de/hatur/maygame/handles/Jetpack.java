package de.hatur.maygame.handles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.sun.org.apache.xml.internal.utils.StopParseException;

import de.hatur.maygame.MayGame;
import de.hatur.maygame.entity.Entity;
import de.hatur.maygame.gfx.GfxAnimation;
import de.hatur.maygame.handles.LevelManager.DeathReason;
import de.hatur.maygame.handles.SoundManager.SoundType;
import de.hatur.maygame.screens.GameScreen;

public class Jetpack implements Entity {
	
	private MayGame game;
	private GameScreen gameScreen;
	private Body body;
	
	public enum Direction {
		TOP,
		LEFT,
		RIGHT,
		BOTTOM
	};
	
	private Vector2 lastPosition;
	private float lastAngle;
	
	private float fFuel;
	private float fLife;
	private boolean bIsJetpackFueled = false;
	private boolean bAttached;
	private boolean bWarningSoundPlayed = false;
	private boolean bDestructionSoundPlayed = false;
	private boolean bStandingStill = false;
	private boolean bTopFueled = false;
	private boolean bSideFueled = false;
	private long standStillStartTime = 0;
	
	// To stop permanent retransform
	private boolean bNeedsAngularStabilization = false;
	
	public final static float MAX_SPEED_Y = 20f;
	public final static float MAX_SPEED_X = 20f;
	private final float SPEED_BOOST_FORCE = 250f;
	public static final float JETPACK_Y_ACCELERATION = 50f;
	public static final float JETPACK_X_ACCELERATION = 25f;
	private final int STAND_STILL_TIME = 2000;
	private final float STAND_STILL_VECLOITY = 0.5f;
	
	public Jetpack(MayGame game, GameScreen gameScreen, Body body, float fStartingFuel) {
		this.game = game;
		this.gameScreen = gameScreen;
		this.body = body;
		
		lastPosition = body.getPosition();
		lastAngle = body.getAngle();
		
		fFuel = fStartingFuel;
		fLife = 100.0f;
		bAttached = true;
		
		Entity.minionList.add(this);
	}
	
	public boolean isJetpackAttached() {
		return bAttached;
	}
	
	/**
	 * de-attach jetpack if destroyed and still attached, called in render
	 */
	public void checkDestruction() {
		if (!isJetpackAlive() && isJetpackAttached()) {
			gameScreen.getWorld().destroyJoint(gameScreen.getPlayerJoint());
			gameScreen.getPlayerJoint().setUserData(new Boolean(false)); // set to false, see gamescreen explanation
			bAttached = false;
		}
	}
	
	public boolean isJetpackAlive() {
		return (fLife > 0.0f) ? true : false;
	}
	
	public boolean hasFuel() {
		return (fFuel > 0.0f) ? true : false;
	}
	
	public float getLife() {
		return (float) Math.ceil(Math.max(0, (Math.round(fLife * 100.0) / 100.0)));
	}
	
	public float getFuel() {
		return (float) (Math.round(fFuel * 100.0) / 100.0);
	}
	
	/**
	 * Used to determine if the jetpack is under a certain velocity for a period of time, to show defeat screen
	 * @return
	 */
	public boolean getFuelEmptyEndFlag() {
		if (body.getLinearVelocity().x <= STAND_STILL_VECLOITY && body.getLinearVelocity().x >= -STAND_STILL_VECLOITY
			&& body.getLinearVelocity().y <= STAND_STILL_VECLOITY && body.getLinearVelocity().y >= -STAND_STILL_VECLOITY) {
			bStandingStill = true;
			if (standStillStartTime == 0)
				standStillStartTime = System.currentTimeMillis();
			
			System.out.println(standStillStartTime);
			
			if (System.currentTimeMillis() >= standStillStartTime + STAND_STILL_TIME) {
				System.out.println("Finish");
				return true;
			}
		}
		else {
			bStandingStill = false;
			standStillStartTime = 0;
		}
		
		return false;
	}
	
	public void applyDamage(float fAmount) {
		fLife -= fAmount;
		
		if (fLife <= 0 && !bDestructionSoundPlayed) {
			game.soundManager.playSound(SoundType.JETPACKDESTRUCTION);
			bDestructionSoundPlayed = true;
			gameScreen.addAnimation(new GfxAnimation(gameScreen.explosionAnimationTexture, body, 0.25f, 1f, 3.5f, 3.5f, 1, 8, 8));
			gameScreen.levelManager.setLevelFinished(false, DeathReason.JETPACKDEAD);
		}
	}
	
	public boolean isTopFueled() {
		return bTopFueled;
	}
	
	public boolean isSideFueled() {
		return bSideFueled;
	}
	
	public void fuel(Direction direction) {
		if (gameScreen.levelManager.isLevelStarted() && !gameScreen.levelManager.isLevelFinished() && isJetpackAttached() && isJetpackAlive()) {
			float fXForce = 0;
			float fYForce = 0;
			bTopFueled = false;
			bSideFueled = false;
			switch(direction) {
			case TOP:
				fYForce = JETPACK_Y_ACCELERATION;
				fFuel -= 0.05f;
				game.soundManager.playFuelingSound(true);
				break;
			case LEFT:
				fXForce = -JETPACK_X_ACCELERATION;
				fFuel -= 0.01f;
				game.soundManager.playFuelingSound(false);
				// play leiser fuzelingsound
				break;
			case RIGHT:
				fXForce = JETPACK_X_ACCELERATION;
				fFuel -= 0.01f;
				game.soundManager.playFuelingSound(false);
				// play leiser fuzelingsound
				break;
			default:
				assert false;
				break;
			}
			gameScreen.getPlayer().getPlayerBody().applyForceToCenter(fXForce, fYForce, true);
			float fDifference = gameScreen.getJetpack().getJetpackBody().getMass() / gameScreen.getPlayer().getPlayerBody().getMass();
			gameScreen.getJetpack().getJetpackBody().applyForceToCenter(fDifference * fXForce, fDifference * fYForce, true);
		}
	}
	
//	/**
//	 * For interpolation
//	 * @param delta
//	 * @param direction
//	 */
//	public void fuel(float delta, Direction direction) {
//		if (gameScreen.levelManager.isLevelStarted() && !gameScreen.levelManager.isLevelFinished() && isJetpackAttached() && isJetpackAlive()) {
//			float fXForce = 0;
//			float fYForce = 0;
//			switch(direction) {
//			case TOP:
//				fYForce = delta * JETPACK_Y_ACCELERATION;
//				fFuel -= 0.05f;
////				game.soundManager.playFuelingSound();
//				break;
//			case LEFT:
//				fXForce = delta * -JETPACK_X_ACCELERATION;
//				fFuel -= 0.01f;
//				// play leiser fuzelingsound
//				break;
//			case RIGHT:
//				fXForce = delta * JETPACK_X_ACCELERATION;
//				fFuel -= 0.01f;
//				// play leiser fuzelingsound
//				break;
//			default:
//				assert false;
//				break;
//			}
//			gameScreen.getPlayer().getPlayerBody().applyForceToCenter(fXForce, fYForce, true);
//			float fDifference = gameScreen.getJetpack().getJetpackBody().getMass() / gameScreen.getPlayer().getPlayerBody().getMass();
//			gameScreen.getJetpack().getJetpackBody().applyForceToCenter(fDifference * fXForce, fDifference * fYForce, true);
//		}
//	}
	
	public void refuel(float fAmount) {
		fFuel += fAmount;
		if (fFuel > 100.0f)
			fFuel = 100.0f;
	}
	
	public void applySpeedBoost(Direction direction) {
		if (gameScreen.levelManager.isLevelStarted() && !gameScreen.levelManager.isLevelFinished()) {
			float fXForce = 0;
			float fYForce = 0;
			switch (direction) {
			case TOP:
				fYForce = SPEED_BOOST_FORCE;
				break;
			case BOTTOM:
				fYForce = -SPEED_BOOST_FORCE;
				break;
			case LEFT:
				fXForce = -SPEED_BOOST_FORCE;
				break;
			case RIGHT:
				fXForce = SPEED_BOOST_FORCE;
				break;
			default:
				assert false;
				break;
			}
			gameScreen.getPlayer().getPlayerBody().applyForceToCenter(fXForce, fYForce, true);
			float fDifference = body.getMass() / gameScreen.getPlayer().getPlayerBody().getMass();
			body.applyForceToCenter(fDifference * fXForce, fDifference * fYForce, true);
		}
	}

	public Body getJetpackBody() {
		return body;
	}
	
	public boolean isFueled() {
		return bIsJetpackFueled;
	}
	
	

	@Override
	public void update() {
		// Fuel jetpack if keys presssed
		if (gameScreen.levelManager.isLevelStarted() && !gameScreen.levelManager.isLevelFinished() && isJetpackAttached() && isJetpackAlive()) {
			if (Gdx.input.isKeyPressed(Keys.SPACE) || gameScreen.getGasButton().isPressed()) {
				if (hasFuel()) {
					fuel(Direction.TOP);
					bIsJetpackFueled = true;
				}
			}
			else {
				bIsJetpackFueled = false;
				game.soundManager.stopFuelingSound();
			}
			
			if (Gdx.input.isKeyPressed(Keys.LEFT) || gameScreen.getLeftButton().isPressed()) {
				if (hasFuel()) {
					fuel(Direction.LEFT);
					bSideFueled = true;
				}
			}
			else if (Gdx.input.isKeyPressed(Keys.RIGHT) || gameScreen.getRightButton().isPressed()) {
				if (hasFuel()) {
					fuel(Direction.RIGHT);
					bSideFueled = true;
				}
			}
			else {
				bSideFueled = false;
			}
		}
		
		// Limit jetpack speed
		// Y-Speed
		if (body.getLinearVelocity().y > MAX_SPEED_Y) {
			body.applyForceToCenter(0, -(Jetpack.JETPACK_Y_ACCELERATION) * (body.getMass() / gameScreen.getPlayer().getPlayerBody().getMass()), true);
		}
		// X-Speed
		if (body.getLinearVelocity().x > MAX_SPEED_X) {
			body.applyForceToCenter(-(Jetpack.JETPACK_X_ACCELERATION) * (body.getMass() / gameScreen.getPlayer().getPlayerBody().getMass()), 0, true);
		}
		else if (body.getLinearVelocity().x < -MAX_SPEED_X) {
			body.applyForceToCenter((Jetpack.JETPACK_X_ACCELERATION) * (body.getMass() / gameScreen.getPlayer().getPlayerBody().getMass()), 0, true);
		}
		
		// Stabilize angle
		float fCurrentAngle = gameScreen.getPlayer().getPlayerBody().getAngle();
		float fAngularVelo = gameScreen.getPlayer().getPlayerBody().getAngularVelocity();
		if (fCurrentAngle > MathUtils.PI2) {
			int iSub = (int) (fCurrentAngle / MathUtils.PI2);
			gameScreen.getPlayer().getPlayerBody().setTransform(gameScreen.getPlayer().getPlayerBody().getPosition(), fCurrentAngle - (iSub * MathUtils.PI2));
			body.setTransform(body.getPosition(), fCurrentAngle - (iSub * MathUtils.PI2));
		}
		else if (fCurrentAngle < -MathUtils.PI2) {
			int iSub = (int) (fCurrentAngle / MathUtils.PI2);
			gameScreen.getPlayer().getPlayerBody().setTransform(gameScreen.getPlayer().getPlayerBody().getPosition(), -1 * (fCurrentAngle - (iSub * MathUtils.PI2)));
			body.setTransform(body.getPosition(), -1 * (fCurrentAngle - (iSub * MathUtils.PI2)));
		}
		
		if (fCurrentAngle > 0.05f && fCurrentAngle < MathUtils.PI) {
			bNeedsAngularStabilization = true;
			gameScreen.getPlayer().getPlayerBody().applyTorque(-0.5f, true);
			body.applyTorque(-0.5f, true);
		}
		else if (fCurrentAngle > MathUtils.PI) {
			bNeedsAngularStabilization = true;
			gameScreen.getPlayer().getPlayerBody().applyTorque(0.5f, true);
			body.applyTorque(0.5f, true);
		}
		else if (fCurrentAngle > -MathUtils.PI && fCurrentAngle < -0.05f) {
			bNeedsAngularStabilization = true;
			gameScreen.getPlayer().getPlayerBody().applyTorque(0.5f, true);
			body.applyTorque(0.5f, true);
		}
		else if (fCurrentAngle < -MathUtils.PI) {
			bNeedsAngularStabilization = true;
			gameScreen.getPlayer().getPlayerBody().applyTorque(-0.5f, true);
			body.applyTorque(-0.5f, true);
		}
		
		if (fCurrentAngle <= 0.05f && fCurrentAngle >= -0.05f && fAngularVelo <= 0.25f && fAngularVelo >= -0.25f && bNeedsAngularStabilization) {
			gameScreen.getPlayer().getPlayerBody().setAngularVelocity(0);
			body.setAngularVelocity(0);
			gameScreen.getPlayer().getPlayerBody().setTransform(gameScreen.getPlayer().getPlayerBody().getPosition(), 0);
			body.setTransform(body.getPosition(), 0);
			bNeedsAngularStabilization = false;
		}
		
		// check if jetpack is out of fuel and stands still (end condition)
		if (!hasFuel() && getFuelEmptyEndFlag())
			gameScreen.levelManager.setLevelFinished(false, DeathReason.NOFUEL);
	}

	@Override
	public void victoryEmote(SpriteBatch batch, BitmapFont font) {
	}

	@Override
	public void saveLastPosition() {
		lastPosition = body.getPosition();
		lastAngle = body.getAngle();
	}
}

package de.hatur.maygame.handles;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.SoundLoader;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.Disposable;

import de.hatur.maygame.MayGame;

public class SoundManager implements Disposable {
	private MayGame game;
	
	public enum SoundType {
		COIN1,
		COIN2,
		COIN3,
		HIT,
		WARNING,
		START,
		REFUEL,
		JETPACKDESTRUCTION,
		DOGEDEFEATED,
		SAWHIT
	};
	
	private Random randomGenerator;
	
	private Sound soundCoin1;
	private Sound soundCoin2;
	private Sound soundCoin3;
	private Sound soundJetpackDestruction;
	private Sound soundDogeDefeated;
	private Sound soundRefuel;
	private Sound soundHit;
	private Sound soundHit2;
	private Sound soundHit3;
	private Sound soundHit4;
	private Sound soundSaw;
	
	private long fuelSound = 0;
	private boolean fuelingPaused = false;
	private long fuelSoundStart = 0;
	private final long fuelSoundLength = 76;
	
	/**
	 * Handling soundeffects, coin sounds etc. - !NOT MUSIC!
	 * @param game
	 */
	public SoundManager(MayGame game) {
		this.game = game;
		randomGenerator = new Random();
		initSound();
	}
	
	/**
	 * used private to load sound files
	 */
	private void initSound() {
		soundCoin1 = Gdx.audio.newSound(Gdx.files.internal("data/audio/coin1.wav"));
		soundCoin2 = Gdx.audio.newSound(Gdx.files.internal("data/audio/coin2.wav"));
		soundCoin3 = Gdx.audio.newSound(Gdx.files.internal("data/audio/coin3.wav"));
		soundJetpackDestruction = Gdx.audio.newSound(Gdx.files.internal("data/audio/jetpackdestroyed.wav"));
		soundDogeDefeated = Gdx.audio.newSound(Gdx.files.internal("data/audio/dogedefeated.wav"));
		soundRefuel = Gdx.audio.newSound(Gdx.files.internal("data/audio/refuel.wav"));
		soundHit = Gdx.audio.newSound(Gdx.files.internal("data/audio/hit.wav"));
		soundHit2 = Gdx.audio.newSound(Gdx.files.internal("data/audio/hit2.wav"));
		soundHit3 = Gdx.audio.newSound(Gdx.files.internal("data/audio/hit3.wav"));
		soundHit4 = Gdx.audio.newSound(Gdx.files.internal("data/audio/hit4.wav"));
		soundSaw = Gdx.audio.newSound(Gdx.files.internal("data/audio/saw.wav"));
	}
	
	public void playSound(SoundType soundType) {
		if (game.optionsManager.isSoundEnabled())
			switch (soundType) {
			case COIN1:
				soundCoin1.play();
				break;
			case COIN2:
				soundCoin2.play();
				break;
			case COIN3:
				soundCoin3.play();
				break;
			case HIT:
				int iRandom = randomGenerator.nextInt(3);
				switch (iRandom) {
				case 0:
					soundHit.play();
					break;
				case 1:
					soundHit2.play();
					break;
				case 2:
					soundHit3.play();
					break;
				case 3:
					soundHit4.play();
					break;
				default:
					assert false;
					break;
				}
				break;
			case JETPACKDESTRUCTION:
				soundJetpackDestruction.play();
				break;
			case DOGEDEFEATED:
				soundDogeDefeated.play();
				break;
			case START:
				break;
			case WARNING:
				break;
			case REFUEL:
				soundRefuel.play();
				break;
			case SAWHIT:
				soundSaw.play();
				break;
			default:
				System.out.println("No such sound: " + soundType);
				break;
			}
	}
	
	public void playFuelingSound(boolean top) {
		
	}
	
	public void stopFuelingSound() {
	}

	@Override
	public void dispose() {
		soundCoin1.dispose();
		soundCoin2.dispose();
		soundCoin3.dispose();
		soundJetpackDestruction.dispose();
		soundDogeDefeated.dispose();
		soundRefuel.dispose();
		soundHit.dispose();
		soundHit2.dispose();
		soundHit3.dispose();
		soundHit4.dispose();
	}
}

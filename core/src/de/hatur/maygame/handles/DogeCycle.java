package de.hatur.maygame.handles;

import com.badlogic.gdx.LifecycleListener;

/**
 * This is a LifeCycleListener to prevent playing/restarting music while the game is minimized in Android
 * @author Hatura
 *
 */
public class DogeCycle implements LifecycleListener {
	
	private boolean gamePaused = false;

	@Override
	public void pause() {
		gamePaused = true;
	}

	@Override
	public void resume() {
		gamePaused = false;
	}

	@Override
	public void dispose() {
	}
	
	public boolean isGamePaused() {
		return gamePaused;
	}

}

package de.hatur.maygame.handles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;

/**
 * DogeData is the intern data type for the body, it extends the information of the body for game needs
 * @author Hatura
 *
 */
public class DogeData {
	private Body body;
	private String sType;
	private boolean bIsSolid;
	private boolean bIsSprite = false;
	private Sprite sprite;
	private boolean bIsAnimation = false;
	private boolean bIsReadyForBox2DDeletion = false;
	private boolean bIsActive = true;
	
	// For moveable Coins
	private boolean bIsMovableCoin = false;
	private boolean bIsCollided = false;
	
	// For sprite animations
	private Animation animation;
	private TextureRegion[] trFrames;
	private float textureWidth = 0;
	private float textureHeight = 0;
	
	// Fake animation for buzz saw blades
	private float spriteAngle = 0;
	
	private float previousX;
	private float previousY;
	private float previousA;
	
	public DogeData() {
		this.sType = "undefined";
		this.bIsSolid = false;
	}
	
	public DogeData(Body body, String sType, boolean bIsSolid) {
		this.body = body;
		this.sType = sType;
		this.bIsSolid = bIsSolid;
	}
	
	/**
	 * Object has sprite
	 * @param sType
	 * @param bIsSolid
	 * @param sprite
	 */
	public DogeData(Body body, String sType, boolean bIsSolid, Sprite sprite) {
		this.body = body;
		this.sType = sType;
		this.bIsSolid = bIsSolid;
		this.bIsSprite = true;
		this.sprite = sprite;
	}
	
	/**
	 * Object has animated sprite using animation and Sprite sheet file
	 * @param sType
	 * @param bIsSolid
	 * @param animationTexture
	 * @param cells - to X
	 * @param rows - to Y
	 */
	public DogeData(Body body, String sType, boolean bIsSolid, Texture animationTexture, float width, float height, int cells, int rows) {
		this.body = body;
		this.sType = sType;
		this.bIsSolid = bIsSolid;
		this.bIsAnimation = true;
		
		TextureRegion[][] trTmp = TextureRegion.split(animationTexture, animationTexture.getWidth() / cells, animationTexture.getHeight() / rows);
		trFrames = new TextureRegion[cells * rows];
		
		int index = 0;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cells; j++) {
				trFrames[index++] = trTmp[i][j];
			}
		}
		
		animation = new Animation(1f/29f, trFrames);
		textureWidth = width;
		textureHeight = height;
	}
	
	/**
	 * The unique identifier
	 * @return
	 */
	public String getType() {
		return sType;
	}
	
	public boolean isSolid() {
		return bIsSolid;
	}
	
	public boolean isSprite() {
		return bIsSprite;
	}
	
	public Sprite getSprite() {
		return sprite;
	}
	
	public boolean isAnimation() {
		return bIsAnimation;
	}
	
	/**
	 * for animated sprite -> add delta and get next key frame
	 * @param delta
	 * @return current frame to draw for animation
	 */
	public TextureRegion getFrame(float fStateTime) {
		return animation.getKeyFrame(fStateTime, true);
	}
	
	/**
	 * Only for animated sprite!
	 * @return
	 */
	public float getTextureWidth() {
		return textureWidth;
	}
	
	/**
	 * Only for animated sprite!
	 * @return
	 */
	public float getTextureHeight() {
		return textureHeight;
	}
	
	public boolean isActive() {
		return bIsActive;
	}
	
	public void disableObject() {
		sprite = null;
		bIsSprite = false;
		bIsActive = false;
	}
	
	public void deleteObject() {
		sprite = null;
		bIsSprite = false;
		bIsReadyForBox2DDeletion = true;
	}
	
	/**
	 * For moveable coins, to improve behaviour (add check) if 2 coins are affected
	 */
	public boolean isMovableCoin() {
		return bIsMovableCoin;
	}
	
	/**
	 * For moveable coins, to improve behaviour (add check) if 2 coins are affected
	 */
	public void setMovableCoin() {
		bIsMovableCoin = true;
	}
	
	/**
	 * For moveable coins, detection handling cause sensors dont stop at static environment (bodies)
	 */
	public boolean isCollided() {
		return bIsCollided;
	}
	
	/**
	 * For moveable coins, detection handling cause sensors dont stop at static environment (bodies)
	 */
	public void setCollided(boolean bIsCollided) {
		this.bIsCollided = bIsCollided;
	}
	
	/**
	 * flag for Box2D deletion
	 */
	public boolean isReadyForBox2DDeletion() {
		return bIsReadyForBox2DDeletion;
	}
	
	public float getSpriteAngle() {
		return spriteAngle;
	}
	
	/**
	 * For render interpolation for spritepositions, i have no idea if this is necessary..
	 */
	public void saveInterpolationPosition() {
		previousX = body.getPosition().x;
		previousY = body.getPosition().y;
		previousA = body.getAngle() * MathUtils.radiansToDegrees;
	}
	/**
	 * For interpolation only
	 * @return
	 */
	
	public float getPreviousX() {
		return previousX;
	}
	
	/**
	 * For interpolation only
	 * @return
	 */
	public float getPreviousY() {
		return previousY;
	}
	
	/**
	 * For interpolation only
	 * @return
	 */
	public float getPreviousAngle() {
		return previousA;
	}
}

package de.hatur.maygame.screens;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import de.hatur.maygame.MayGame;
import de.hatur.maygame.particle.Particle.StandardType;
import de.hatur.maygame.particle.Particle.TransformationType;
import de.hatur.maygame.particle.ParticleManager;

public class OptionsScreen implements Screen {
	
	private MayGame game;
	private Stage stage;
	
	private Group backgroundGroup;
	private Table tableOptionsBackground;
	private Table tableOptionsSelectables;
	
	private ParticleManager particleManager;
	
	private Random randomGenerator;
	
	public OptionsScreen(MayGame game) {
		this.game = game;
	}

	@Override
	public void render(float delta) {
		Gdx.graphics.getGL20().glClearColor(0, 0, 0, 1);
		Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		
		stage.act();
		stage.draw();
		
		StandardType randomType = null;
		int iRandom = randomGenerator.nextInt(2);
		switch (iRandom) {
		case 0:
			randomType = StandardType.LEFT_RIGHT;
			break;
		case 1:
			randomType = StandardType.RIGHT_LEFT;
			break;
		default:
			randomType = StandardType.LEFT_RIGHT;
			break;
		}
		
		if (particleManager.isStarted())
			particleManager.manage(delta, 0.35f, game.gfx.getDrawable("screwdriver"), 10, randomType, TransformationType.RANDOM_ROTATION, Interpolation.circleOut);
		
		// To avoid delta fluctuations from first delta (long one) in first frame
		if (!particleManager.isStarted())
			particleManager.start();
		
//		Table.drawDebug(stage);
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show() {
		randomGenerator = new Random();
		
		stage = new Stage(new StretchViewport(MayGame.VIRTUAL_WIDTH, MayGame.VIRTUAL_HEIGHT));
		
		InputMultiplexer multiplexer = new InputMultiplexer(game.backprocessor, stage);
		Gdx.input.setInputProcessor(multiplexer);
		
		backgroundGroup = new Group();
		particleManager = new ParticleManager(backgroundGroup);
		
		createInterface();
		
		game.getRequestHandler().showAds(true);
	}

	@Override
	public void hide() {
		this.dispose();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		particleManager.dispose();
		stage.dispose();
	}
	
	private void createInterface() {
		tableOptionsBackground = new Table();
		tableOptionsSelectables = new Table();
		
		// Background UI
		
		TextButton tbBack = new TextButton("BACK", game.skin, "bigdoge");
		tbBack.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(new MainmenuScreen(game));
			}
		});
		
		tableOptionsBackground.setSize(480, 360);
		tableOptionsBackground.setPosition((MayGame.VIRTUAL_WIDTH / 2) - (tableOptionsBackground.getWidth() / 2), (MayGame.VIRTUAL_HEIGHT / 2) - (tableOptionsBackground.getHeight() / 2)+20);
		tableOptionsBackground.pad(0);
		tableOptionsBackground.top().center();
		tableOptionsBackground.add(new Image(game.gfx.getDrawable("screwdriver_big"))).padRight(16);
		tableOptionsBackground.add(new Label("Options", game.skin, "dogeinfo32")).align(Align.center);
		tableOptionsBackground.row().padTop(6);
		tableOptionsBackground.add().colspan(2).expandY();
		tableOptionsBackground.row().padTop(6);
		tableOptionsBackground.add(tbBack).colspan(2).width(200).height(60).padBottom(24);
		
//		tableOptionsBackground.debug();
		
		// Actual Options
		
		TextButton tbSoundOn = new TextButton("On", game.skin, "bigdoge_dynamic");
		TextButton tbSoundOff = new TextButton("Off", game.skin, "bigdoge_dynamic");
		TextButton tbMusicOn = new TextButton("On", game.skin, "bigdoge_dynamic");
		TextButton tbMusicOff = new TextButton("Off", game.skin, "bigdoge_dynamic");
		TextButton tbGraphicsLow = new TextButton("Low", game.skin, "bigdoge_dynamic");
		TextButton tbGraphicsHigh = new TextButton("High", game.skin, "bigdoge_dynamic");
		
		tbSoundOn.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.optionsManager.toggleSoundEnabled();
			}
		});
		
		tbSoundOff.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.optionsManager.toggleSoundEnabled();
			}
		});
		
		tbMusicOn.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.optionsManager.toggleMusicEnabled();
			}
		});
		
		tbMusicOff.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.optionsManager.toggleMusicEnabled();
			}
		});
		
		tbGraphicsLow.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.optionsManager.toggleGraphics();
			}
		});
		
		tbGraphicsHigh.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.optionsManager.toggleGraphics();
			}
		});
		
		ButtonGroup bgSound = new ButtonGroup(tbSoundOn, tbSoundOff);
		ButtonGroup bgMusic = new ButtonGroup(tbMusicOn, tbMusicOff);
		ButtonGroup bgGraphics = new ButtonGroup(tbGraphicsLow, tbGraphicsHigh);
		
		bgSound.setMaxCheckCount(1);
		bgSound.setMinCheckCount(1);
		
		if (game.optionsManager.isSoundEnabled())
			tbSoundOn.setChecked(true);
		else
			tbSoundOff.setChecked(true);
		
		bgMusic.setMaxCheckCount(1);
		bgMusic.setMinCheckCount(1);
		
		if (game.optionsManager.isMusicEnabled())
			tbMusicOn.setChecked(true);
		else
			tbMusicOff.setChecked(true);
		
		bgGraphics.setMaxCheckCount(1);
		bgGraphics.setMinCheckCount(1);
		
		if (game.optionsManager.isGraphicsHigh())
			tbGraphicsHigh.setChecked(true);
		else
			tbGraphicsLow.setChecked(true);
		
		tableOptionsSelectables.setSize(480, 360);
		tableOptionsSelectables.setPosition((MayGame.VIRTUAL_WIDTH / 2) - (tableOptionsBackground.getWidth() / 2), (MayGame.VIRTUAL_HEIGHT / 2) - (tableOptionsBackground.getHeight() / 2)+30);
		tableOptionsSelectables.pad(0);
		tableOptionsSelectables.top().center();
		tableOptionsSelectables.add(new Label("Sound:", game.skin, "dogeinfo32")).width(110);
		tableOptionsSelectables.add(tbSoundOn).width(100).height(50);
		tableOptionsSelectables.add(tbSoundOff).width(100).height(50).padLeft(5);
		tableOptionsSelectables.row().padTop(6);
		tableOptionsSelectables.add(new Label("Music:", game.skin, "dogeinfo32")).width(110);
		tableOptionsSelectables.add(tbMusicOn).width(100).height(50);
		tableOptionsSelectables.add(tbMusicOff).width(100).height(50).padLeft(5);
		tableOptionsSelectables.row().padTop(6);
		tableOptionsSelectables.add(new Label("Graphics:", game.skin, "dogeinfo32")).width(110);
		tableOptionsSelectables.add(tbGraphicsLow).width(100).height(50);
		tableOptionsSelectables.add(tbGraphicsHigh).width(100).height(50).padLeft(5);
		
//		tableOptionsSelectables.debug();
		
		stage.addActor(backgroundGroup);
		stage.addActor(tableOptionsBackground);
		stage.addActor(tableOptionsSelectables);
	}

}

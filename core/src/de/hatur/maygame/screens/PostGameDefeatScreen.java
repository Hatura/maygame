package de.hatur.maygame.screens;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import de.hatur.maygame.MayGame;
import de.hatur.maygame.handles.LevelManager;

public class PostGameDefeatScreen implements Screen {
	
	private MayGame game;
	private LevelManager levelManager;
	
	private Stage stage;
	private Table tablePostGame;
	
	private ArrayList<String> defeatScreenText;
	private Random randomGenerator;
	
	public PostGameDefeatScreen(MayGame game, LevelManager levelManager) {
		this.game = game;
		this.levelManager = levelManager;
	}

	@Override
	public void render(float delta) {
		Gdx.graphics.getGL20().glClearColor(0, 0, 0, 1);
		Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		
		stage.act();
		stage.draw();
		
		Table.drawDebug(stage);
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show() {
		stage = new Stage(new StretchViewport(MayGame.VIRTUAL_WIDTH, MayGame.VIRTUAL_HEIGHT));
		
		InputMultiplexer multiplexer = new InputMultiplexer(game.backprocessor, stage);
		Gdx.input.setInputProcessor(multiplexer);
		
		createInterface();
		
		game.getRequestHandler().showAds(true);
	}

	@Override
	public void hide() {
		this.dispose();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		stage.dispose();
	}
	
	private void createInterface() {
		tablePostGame = new Table();
		
		TextButton tbRestart = new TextButton("Restart level", game.skin, "bigdoge");
		TextButton tbBack = new TextButton("To main menu", game.skin, "bigdoge");
		
		tbBack.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(new MainmenuScreen(game));
			}
		});
		
		tbRestart.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(new GameScreen(game, levelManager.getCurrentLevel()));
			}
		});
		
		HorizontalGroup hgHeadingHack = new HorizontalGroup();
		hgHeadingHack.addActor(new Image(game.gfx.getDrawable("loss")));
		hgHeadingHack.addActor(new Label(" DEFEAT!", game.skin, "dogeinfo64"));
		
		randomGenerator = new Random();
		defeatScreenText = new ArrayList<String>();
		defeatScreenText.add("Such unlucky, very embarrassing");
		defeatScreenText.add("So pain, much hurt doge");
		defeatScreenText.add("Wow");
		defeatScreenText.add("Wow, many down to earth");
		defeatScreenText.add("Am sad now");
		defeatScreenText.add("Many unlucky");
		int iRandom = randomGenerator.nextInt(defeatScreenText.size());
		
		tablePostGame.setSize(480, 360);
		tablePostGame.setPosition((MayGame.VIRTUAL_WIDTH / 2) - (tablePostGame.getWidth() / 2), (MayGame.VIRTUAL_HEIGHT / 2) - (tablePostGame.getHeight() / 2)+36);
		tablePostGame.pad(0);
		tablePostGame.top().center();
		tablePostGame.add(hgHeadingHack).colspan(2);
		tablePostGame.row();
		tablePostGame.add().colspan(2).expandY();
		tablePostGame.row().padTop(6);
		tablePostGame.add(new Label(defeatScreenText.get(iRandom), game.skin, "dogeinfo32")).colspan(2);
		tablePostGame.row();
		tablePostGame.add().colspan(2).expandY();
		tablePostGame.row().padTop(6);
		tablePostGame.add(tbRestart).width(200).height(80);
		tablePostGame.add(tbBack).width(200).height(80).padLeft(12);
		
//		tablePostGame.debug();
		
		stage.addActor(tablePostGame);
	}
}

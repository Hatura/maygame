package de.hatur.maygame.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import de.hatur.maygame.MayGame;
import de.hatur.maygame.particle.Particle.StandardType;
import de.hatur.maygame.particle.ParticleManager;

public class MainmenuScreen implements Screen {
	
	private MayGame game;
	
	private Stage stage;
	
	private Group backgroundGroup;
	private Table tableMainmenu;
	
	private ParticleManager particleManager;
	
	public MainmenuScreen(MayGame game) {
		this.game = game;
	}

	@Override
	public void render(float delta) {
		Gdx.graphics.getGL20().glClearColor(0, 0, 0, 1);
		Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		
		stage.act();
		stage.draw();
		
		if (particleManager.isStarted())
			particleManager.manage(delta, 0.05f, game.gfx.getDrawable("star"), (int) (Math.random() * 5 + 3), StandardType.TOP_BOTTOM, null, null);
		
		// To avoid delta fluctuations from first delta (long one) in first frame
		if (!particleManager.isStarted())
			particleManager.start();
		
		Table.drawDebug(stage);
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show() {
		stage = new Stage(new StretchViewport(MayGame.VIRTUAL_WIDTH, MayGame.VIRTUAL_HEIGHT));
		
		InputMultiplexer multiplexer = new InputMultiplexer(game.backprocessor, stage);
		Gdx.input.setInputProcessor(multiplexer);
		
		backgroundGroup = new Group();
		particleManager = new ParticleManager(backgroundGroup);
		
		createInterface();
		
		game.getRequestHandler().showAds(true);
		
		game.getMusicManager().setMusic_MenuDefault();
	}

	@Override
	public void hide() {
		this.dispose();
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		particleManager.dispose();
		stage.dispose();
	}
	
	private void createInterface() {
		tableMainmenu = new Table();
		
		// NinePatchDrawable tableBackground = new NinePatchDrawable(game.skin.getPatch("menutablebg"));
		
		TextButton tbPlay = new TextButton("PLAY", game.skin, "bigdoge");
		tbPlay.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(new LevelSelectionScreen(game));
//				game.setScreen(new GameScreen(game));
			}
		});
		
		TextButton tbOptions = new TextButton("OPTIONS", game.skin, "bigdoge");
		tbOptions.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(new OptionsScreen(game));
			}
		});
		
		TextButton tbCredits = new TextButton("CREDITS", game.skin, "bigdoge");
		tbCredits.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(new CreditsScreen(game));
			}
		});
		
		TextButton tbExit = new TextButton("EXIT", game.skin, "bigdoge");
		tbExit.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.exit();
			}
		});
		
		tableMainmenu.setSize(480, 360);
		tableMainmenu.setPosition((MayGame.VIRTUAL_WIDTH / 2) - (tableMainmenu.getWidth() / 2), (MayGame.VIRTUAL_HEIGHT / 2) - (tableMainmenu.getHeight() / 2)+20);
		tableMainmenu.pad(0);
		tableMainmenu.top().center();
		tableMainmenu.add(new Image(game.gfx.getDrawable("doge"))).padRight(16);
		tableMainmenu.add(new Label("DOGEQUEST!", game.skin, "dogeinfo64")).align(Align.center);
		tableMainmenu.row().padTop(6);
		tableMainmenu.add(tbPlay).colspan(2).width(200).height(60);
		tableMainmenu.row().padTop(6);
		tableMainmenu.add(tbOptions).colspan(2).width(200).height(60);
		tableMainmenu.row().padTop(6);
		tableMainmenu.add(tbCredits).colspan(2).width(200).height(60);
		tableMainmenu.row().padTop(6);
		tableMainmenu.add(tbExit).colspan(2).width(200).height(60);
		tableMainmenu.row().padTop(4);
		tableMainmenu.add(new Label("Version: " + MayGame.GAME_VERSION + " (Build date: " + MayGame.GAME_VERSION_DATE + ")", game.skin, "white_mini")).colspan(2).align(Align.center);
		
		//tableMainmenu.debug();
		
		stage.addActor(backgroundGroup);
		stage.addActor(tableMainmenu);
		
	}

}

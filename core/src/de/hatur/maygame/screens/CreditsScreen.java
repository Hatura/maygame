package de.hatur.maygame.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import de.hatur.maygame.MayGame;

public class CreditsScreen implements Screen {
	private MayGame game;
	
	private Stage stage;
	private Table tableCreditsBackground;
	private Table tableCredits;
	
	public CreditsScreen(MayGame game) {
		this.game = game;
	}

	@Override
	public void render(float delta) {
		Gdx.graphics.getGL20().glClearColor(0, 0, 0, 1);
		Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		
		stage.act();
		stage.draw();
		
		Table.drawDebug(stage);
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show() {
		stage = new Stage(new StretchViewport(MayGame.VIRTUAL_WIDTH, MayGame.VIRTUAL_HEIGHT));
		
		InputMultiplexer multiplexer = new InputMultiplexer(game.backprocessor, stage);
		Gdx.input.setInputProcessor(multiplexer);
		
		createInterface();
		
		game.getRequestHandler().showAds(true);
	}

	@Override
	public void hide() {
		this.dispose();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		stage.dispose();
	}
	
	private void createInterface() {
		tableCreditsBackground = new Table();
		tableCredits = new Table();
		
		TextButton tbBack = new TextButton("BACK", game.skin, "bigdoge");
		tbBack.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(new MainmenuScreen(game));
			}
		});
		
		tableCreditsBackground.setSize(480, 360);
		tableCreditsBackground.setPosition((MayGame.VIRTUAL_WIDTH / 2) - (tableCreditsBackground.getWidth() / 2), (MayGame.VIRTUAL_HEIGHT / 2) - (tableCreditsBackground.getHeight() / 2)+20);
		tableCreditsBackground.pad(0);
		tableCreditsBackground.top().center();
		tableCreditsBackground.add(new Image(game.gfx.getDrawable("victory"))).width(64).padRight(16);
		tableCreditsBackground.add(new Label("Credits", game.skin, "dogeinfo32")).align(Align.center);
		tableCreditsBackground.row().padTop(6);
		tableCreditsBackground.add().colspan(2).expandY();
		tableCreditsBackground.row().padTop(6);
		tableCreditsBackground.add(tbBack).colspan(2).width(200).height(60).padBottom(24);
		
//		tableCreditsBackground.debug();
		
		tableCredits.setSize(480, 360);
		tableCredits.setPosition((MayGame.VIRTUAL_WIDTH / 2) - (tableCredits.getWidth() / 2), (MayGame.VIRTUAL_HEIGHT / 2) - (tableCredits.getHeight() / 2)+20);
		tableCredits.pad(0);
		tableCredits.top().center();
		tableCredits.add(new Label("\"DOGEQUEST\" - a game by Dennis Gerdes", game.skin)).padTop(-20);
		tableCredits.row();
		tableCredits.add(new Label("Want to learn more about the digital currency Dogecoin?", game.skin, "heading")).padTop(4);
		tableCredits.row();
		tableCredits.add(new Label("Go to www.dogecoin.com or www.reddit.com/r/dogecoin to get started!", game.skin));
		tableCredits.row();
		tableCredits.add(new Label("Credits and thanks to:", game.skin, "heading")).padTop(4);
		tableCredits.row();
		tableCredits.add(new Label("(Music)", game.skin)).padTop(-4);
		tableCredits.row();
		tableCredits.add(new Label("\"Flags\" by Alexander Ehlers (menu)", game.skin)).padTop(-4);
		tableCredits.row();
		tableCredits.add(new Label("\"At Last\" by Android128 (level 8, http://vk.com/android128music)", game.skin)).padTop(-8);
		tableCredits.row();
		tableCredits.add(new Label("\"Sadness\" by Joseph Gilbert / Kistol (game over)", game.skin)).padTop(-8);
		tableCredits.row().padTop(16);
		
//		tableShop.debug();
		
		stage.addActor(tableCreditsBackground);
		stage.addActor(tableCredits);
	}
}

package de.hatur.maygame.screens;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.EllipseMapObject;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Ellipse;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.JointDef.JointType;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.WeldJointDef;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import de.hatur.maygame.MayGame;
import de.hatur.maygame.entity.Dolan;
import de.hatur.maygame.entity.Entity;
import de.hatur.maygame.entity.Entity.EntityDirection;
import de.hatur.maygame.entity.BuzzSaw;
import de.hatur.maygame.entity.Flycat;
import de.hatur.maygame.entity.Groundcat;
import de.hatur.maygame.entity.MoveableCoin;
import de.hatur.maygame.gfx.GfxAnimation;
import de.hatur.maygame.gfx.JetpackAnimation;
import de.hatur.maygame.handles.ContactHandle;
import de.hatur.maygame.handles.Countdown;
import de.hatur.maygame.handles.DogeData;
import de.hatur.maygame.handles.DogeEmote;
import de.hatur.maygame.handles.InputHandle;
import de.hatur.maygame.handles.Jetpack;
import de.hatur.maygame.handles.LevelManager;
import de.hatur.maygame.handles.LevelManager.DeathReason;
import de.hatur.maygame.handles.MusicManager.MusicType;
import de.hatur.maygame.handles.Player;
import de.hatur.maygame.handles.ShaderManager;
import de.hatur.maygame.handles.ShaderManager.Shader;

public class GameScreen implements Screen {
	
	private MayGame game;
	private int iLevel;
	
	private OrthographicCamera camera;
	private OrthographicCamera entitySupportCamera; // Renders only the bitcoins, cats etc. that are in an extended camera viewport to save resources
	
	private int iCameraScaleFactor;

	private boolean cameralocked = false;
	
	// Used to determine the scale factor relative to the high, cause of landscape portrayal
	private float fScaleFactor;
	
	// Box2d Stuff
	private Box2DDebugRenderer box2dDebugRenderer;
	private World world;
	private Body jetpackBody;
	private Body playerBody;
	private Joint playerJoint;
	
	private Array<Body> tmpBodies = new Array<Body>();
	private Array<Joint> tmpJoints = new Array<Joint>();
	private Vector2 playerLastLocation;
	public static final String DEADOBJECT = "DEAD";
	
	private Texture gameBackground;
	private Sprite gameBackgroundSprite;
	private SpriteBatch batch;
	private Sprite playerSprite;
	private Sprite jetpackSprite;
	private Texture rocketfuelAnimationTexture;
	private Texture dogecoinAnimationTexture;
	private Texture litecoinAnimationTexture;
	private Texture bitcoinAnimationTexture;
	public Texture explosionAnimationTexture;
	private BitmapFont dogeFont;
	float fCoinSpinTime = 0f;
	
	// Tilemap
	private TiledMap tiledMap;
	private OrthogonalTiledMapRenderer tmr;
	private float fTiledMapScaleFactor = 32f;
	
	// Scene2d Stuff
	private Stage stage;
	private Table tableDebug;
	private Label labelFPS;
	private Label labelCamera;
	private Label labelXSpeed;
	private Label labelYSpeed;
	private Label labelAngleSpeed;
	private Label labelStat1;
	private Label labelStat2;
	private Label labelStat3;
	
	private TextButton gasButton;
	private TextButton leftButton;
	private TextButton rightButton;
	private Image tankInner;
	private TextButton countdownOverlay;
	private TextButton lossContinueButton;
	private Table tableUIInfo;
	private Label labelTimePassed;
	private Label labelDogeInfo;
	private Label labelBitInfo;
	private Label labelLiteInfo;
	private Label labelJetpackLifeInfo;
	private Label labelPlayerLifeInfo;
	private Label labelFuelInfo;
	
	private boolean overlayFadeOutActionDone = false;
	private float fMaxTankHeight;
	
	// Game logic
	private double accumulator;
	private double currentTime;
	public final static float TARGET_FPS = 60.0f;
	private float step = 1.0f / TARGET_FPS;
	private final float SCENE_HEIGHT = 1000f;
	private final float SCENE_WIDTH = 40f;

	private boolean bDefeatPopupShown = false;
	
	// Custom classes
	private ShaderManager worldShaderManager;
	private Jetpack jetpack;
	public Player player;
	private JetpackAnimation jetpackAnimation;
	public LevelManager levelManager;
	public DogeEmote dogeEmote;
	private Countdown countdown;
	
	// animation container
	ArrayList<GfxAnimation> gfxAnimationList;

	public GameScreen (MayGame game, int iLevel) {
		this.game = game;
		this.iLevel = iLevel;
	}
	
	@Override
	public void render(float delta) {	
		Gdx.graphics.getGL20().glClearColor(0, 0, 0, 1);
		Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		
		// Check if game has started, start if timer is zero
		if (!levelManager.isLevelStarted() && countdown.isFinished())
			levelManager.startLevel();
		
		// Box2d World Update
		double newTime = TimeUtils.millis() / 1000.0;
		double frameTime = Math.min(newTime - currentTime, 0.25);
		float deltaTime = (float) frameTime;
		accumulator += deltaTime;
		
		currentTime = newTime;
		
		// Do while to guarantee one physics step .. else stuttering occours
		do {
			world.step(step, 8, 3);
			accumulator -= step;
			if (levelManager.isLevelStarted() && !levelManager.isLevelFinished()) {
				for (Entity entity : Entity.minionList) {
					entity.update();
					entity.saveLastPosition();
				}
			}
		} while(accumulator >= step);
		
		// for interpolation
		float alpha = (float) (accumulator / step);
		
		// save previous positions for interpolation - is this calculated the right way? isn't body.pos = body.pos later? TODO
		world.getBodies(tmpBodies);
		for (Body body : tmpBodies) {
			DogeData dd = (DogeData) body.getUserData();
			if (dd.isSprite())
				dd.saveInterpolationPosition();
		}
		
//		box2dDebugRenderer.render(world, camera.combined);
		
		// coin spin index
		fCoinSpinTime += deltaTime;
		
		jetpack.checkDestruction();
		
		// Updates / Recalculation
		
		updateCamera();
		
//		box2dDebugRenderer.render(world, camera.combined);
		
		// game background
		gameBackgroundSprite.setPosition(camera.position.x - camera.viewportWidth / 2, camera.position.y - camera.viewportHeight / 2);
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		gameBackgroundSprite.draw(batch);
		batch.end();
		
		// TiledMap
		// only use shader if graphics high
		if (game.optionsManager.isGraphicsHigh())
			worldShaderManager.updateShaderEffects(deltaTime);
		tmr.setView(camera);
		tmr.render();
		
		// Sprites
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		// Manage Box2D-Bodies
		ArrayList<Body> enemySpriteList = new ArrayList<Body>(); // to ensure they draw after everything else
		for (int i = 0; i < tmpBodies.size; i++) {
			Body body = tmpBodies.get(i);
			
			// Clear up dead bodies here, to save cpu power - NOT USED ATM
			// DONT DELETE
//			if (body.getUserData() != null) {
//				DogeData dd = (DogeData) body.getUserData();
//				if (dd.isReadyForBox2DDeletion()) {
//					System.out.println("Destryoing body");
//					world.destroyBody(body);
//				}
//			}
			
			// draw sprites
			DogeData dd = (DogeData) body.getUserData();
			
			Vector3 bodyPositionVector = new Vector3(body.getPosition().x, body.getPosition().y, 0);
			
			// If enemy, add to ArrayList and draw later
			// also render only the stuff thats in the extended camera viewport
			if ((dd.getType().equals("groundcat") || dd.getType().equals("flycat") || dd.getType().equals("dolan")) && entitySupportCamera.frustum.pointInFrustum(bodyPositionVector))
				enemySpriteList.add(body);
			
			else if (dd != null && dd.isSprite() && dd.isActive() && entitySupportCamera.frustum.pointInFrustum(bodyPositionVector)) {
				Sprite sprite = dd.getSprite();
				
				float interpX = body.getPosition().x * alpha + dd.getPreviousX() * (1.0f - alpha);
				float interpY = body.getPosition().y * alpha + dd.getPreviousY() * (1.0f - alpha);
				float interpA = (body.getAngle() * MathUtils.radiansToDegrees) * alpha + dd.getPreviousAngle() * (1.0f - alpha);
				
				sprite.setPosition(interpX - sprite.getWidth() / 2, interpY - sprite.getHeight() / 2);
				sprite.setRotation(interpA);
				//if (!sprite.equals(jetpackSprite) && !sprite.equals(jetpackFueledSprite))
				sprite.draw(batch);
			}
			else if (dd != null && dd.isAnimation() && dd.isActive() && entitySupportCamera.frustum.pointInFrustum(bodyPositionVector)) {
				batch.draw(dd.getFrame(fCoinSpinTime), body.getPosition().x - dd.getTextureWidth() / 2, body.getPosition().y - dd.getTextureHeight() / 2, dd.getTextureWidth(), dd.getTextureHeight());
			}
		}
		
		// Draw previously saved enemies
		for (int i = 0; i < enemySpriteList.size(); i++) {
			Body body = enemySpriteList.get(i);
			DogeData ddEnemy = (DogeData) body.getUserData();
			Sprite sprite = ddEnemy.getSprite();
			sprite.setPosition(body.getPosition().x - sprite.getWidth() / 2, body.getPosition().y - sprite.getHeight() / 2);
			sprite.setRotation(body.getAngle() * MathUtils.radiansToDegrees);
			sprite.draw(batch);
		}
		
		// Draw animations
		for (int i = 0; i < gfxAnimationList.size(); i++) {
			GfxAnimation anim = gfxAnimationList.get(i);
			Sprite sprite = anim.getKeyFrame(deltaTime);
			sprite.draw(batch);
			if (!anim.isActive())
				gfxAnimationList.remove(i);
		}
		
		if (levelManager.isLevelStarted() && !levelManager.isLevelFinished()) {
			Sprite animationSprite = jetpackAnimation.getFrame(deltaTime);
			if (animationSprite != null) {
				animationSprite.draw(batch);
			}
		}
			
		
		// Draw doge emotes
		if (dogeEmote.isActive())
			dogeEmote.drawEmote(playerBody.getPosition());
		
		// Draw dolan stuff
		if (levelManager.isLevelStarted() && !levelManager.isLevelFinished()) {
			for (int i = 0; i < Dolan.dolanList.size(); i++) {
				if (Dolan.dolanList.get(i).wantsToEmote()) {
					Dolan.dolanList.get(i).doDolanEmote(batch, dogeFont);
				}
			}
		}
		
		// Draw cat emotes if game is lost
		if (levelManager.isLevelFinished() && !levelManager.isResultVictory()) {
			for (int i = 0; i < Entity.minionList.size(); i++)
				Entity.minionList.get(i).victoryEmote(batch, dogeFont);
		}
		
		batch.end();
		
		// Fade screen to sepia if level is lost || only when graphics high / shader enabled
		if (levelManager.isLevelFinished() && !levelManager.isResultVictory() && game.optionsManager.isGraphicsHigh()) {
			worldShaderManager.default_getSepiaIntensity(worldShaderManager.default_setSepiaIntensity() + deltaTime / 1.5f); // 1.5 is the timemodifier
		}
		
		// Overlay
		updateStage();
		
		stage.act();
		stage.draw();
		
//		Table.drawDebug(stage);
		
		// check if game is finished
		if (levelManager.isLevelFinished() && levelManager.isResultVictory())
			game.setScreen(new PostGameVictoryScreen(game, levelManager));
		else if (levelManager.isLevelFinished() && !levelManager.isResultVictory() && levelManager.isReadyForDefeatScreen())
			game.setScreen(new PostGameDefeatScreen(game, levelManager));
		
		if (Gdx.input.isKeyPressed(Keys.F3))
			game.setScreen(new PostGameVictoryScreen(game, levelManager));
	}
	
	private void updateCamera() {
		if (!cameralocked && !levelManager.isLevelFinished() && !levelManager.isResultVictory()) {
			camera.position.set(playerBody.getPosition().x, playerBody.getPosition().y, 0);
			camera.update();
			
			entitySupportCamera.position.set(playerBody.getPosition().x, playerBody.getPosition().y, 0);
			entitySupportCamera.update();
		}
	}
	
	private void updateStage() {
		// Start overlay
		if (!countdown.isFinished()) {
			String countdownString = countdown.getTime();
			if (countdownString.equals("0"))
				countdownString = "TO THE MOON!";
			countdownOverlay.setText(countdownString);
		}
		else {
			if (!overlayFadeOutActionDone) {
				overlayFadeOutActionDone = true;
				fadeOutOverlay();
			}
		}
		
		// Top right info
		labelTimePassed.setText("Time: " + levelManager.getTimePassed());
		labelDogeInfo.setText("Dogecoin: " + levelManager.getCollectedDogecoin() + " / " + levelManager.getTotalDogecoin());
		if (levelManager.getCollectedBitcoin() > 0)
			labelBitInfo.setText("Bitcoin: " + levelManager.getCollectedBitcoin());
		if (levelManager.getCollectedLitecoin() > 0)
			labelLiteInfo.setText("Litecoin: " + levelManager.getCollectedLitecoin());
		
		// life info
		int iPlayerLife = (int) player.getLife();
		int iJetpackLife = (int) jetpack.getLife();
		
		if (iPlayerLife > 40)
			labelPlayerLifeInfo.setColor(Color.WHITE);
		else if (iPlayerLife > 15)
			labelPlayerLifeInfo.setColor(Color.YELLOW);
		else if (iPlayerLife >= 0)
			labelPlayerLifeInfo.setColor(Color.RED);
		
		if (iJetpackLife > 40)
			labelJetpackLifeInfo.setColor(Color.WHITE);
		else if (iJetpackLife > 15)
			labelJetpackLifeInfo.setColor(Color.YELLOW);
		else if (iJetpackLife >= 0)
			labelJetpackLifeInfo.setColor(Color.RED);
		
		labelPlayerLifeInfo.setText(String.valueOf(iPlayerLife));
		labelJetpackLifeInfo.setText(String.valueOf(iJetpackLife));
		
		// fuel info
		tankInner.setBounds(tankInner.getX(), tankInner.getY(), tankInner.getWidth(), (fMaxTankHeight * jetpack.getFuel()) / 100);
			
		if (tableDebug.isVisible()) {		
			labelFPS.setText("FPS: " + Gdx.graphics.getFramesPerSecond());
			labelCamera.setText("Camera-Zoom-Factor: " + iCameraScaleFactor);
			
			// Ignore small values
			float fXVelocity = playerBody.getLinearVelocity().x;
			if (fXVelocity < 0.1f && fXVelocity > -0.1f)
				fXVelocity = 0.0f;
			
			float fYVelocity = -1 * playerBody.getLinearVelocity().y;
			if (fYVelocity < 0.1f && fYVelocity > -0.1f)
				fYVelocity = 0.0f;
			
			float fAngleVelocity = playerBody.getAngularVelocity();
			if (fAngleVelocity < 0.1f && fAngleVelocity > -0.1f)
				fAngleVelocity = 0.0f;
			
			labelXSpeed.setText("X-Speed: " + fXVelocity);
			labelYSpeed.setText("Y-Speed: " + fYVelocity);
			labelAngleSpeed.setText("A-Speed: " + fAngleVelocity + ", currentAngle: " + playerBody.getAngle());
			// only use shader if graphics high
			if (game.optionsManager.isGraphicsHigh())
				labelStat2.setText(worldShaderManager.toString());
			else
				labelStat2.setText("No Shader enabled");
		}
	}

	@Override
	public void resize(int width, int height) {
		camera.viewportWidth = width / iCameraScaleFactor;
		camera.viewportHeight = height / iCameraScaleFactor;
		camera.update();
		
		entitySupportCamera.viewportWidth = Gdx.graphics.getWidth() / (iCameraScaleFactor / 4);
		entitySupportCamera.viewportHeight = Gdx.graphics.getHeight() / (iCameraScaleFactor / 4);
		entitySupportCamera.update();
	}

	@Override
	public void show() {
		// Inits
		fScaleFactor = (Gdx.graphics.getHeight() / MayGame.REFERENCE_HEIGHT);
		
		stage = new Stage(new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
		
		InputHandle gamescreenInput = new InputHandle(this);
		
		InputMultiplexer multiplexer = new InputMultiplexer(game.backprocessor, gamescreenInput, stage);
		Gdx.input.setInputProcessor(multiplexer);
		
		// Setup shaderManager and sysout errors if compilation failed | only use shader if graphics high
		if (game.optionsManager.isGraphicsHigh())
			worldShaderManager = new ShaderManager(Shader.DEFAULT);
		
		batch = new SpriteBatch();
		// only use shader if graphics high
		if (game.optionsManager.isGraphicsHigh())
			batch.setShader(worldShaderManager.getShaderProgram());
		
		// load level data
		levelManager = new LevelManager(game, this, iLevel);
		levelManager.setItems(
				game.saveManager.levelData.get(iLevel).getTotalDogecoin(),
				game.saveManager.levelData.get(iLevel).getTotalBitcoin(),
				game.saveManager.levelData.get(iLevel).getTotalLitecoin()
				);
		
		camera = new OrthographicCamera();
		entitySupportCamera = new OrthographicCamera();
		
		iCameraScaleFactor = (int) (40 * fScaleFactor);
		
		// Textures
		gameBackground = new Texture(Gdx.files.internal("data/img/gamebackground/genbg.png"));
		gameBackground.setFilter(game.optionsManager.getGraphicsParams(), game.optionsManager.getGraphicsParams());
		
		gameBackgroundSprite = new Sprite(gameBackground);
		gameBackgroundSprite.setSize(Gdx.graphics.getWidth() / iCameraScaleFactor, Gdx.graphics.getHeight() / iCameraScaleFactor);
		
		System.out.println(gameBackgroundSprite.getWidth());
		System.out.println(gameBackgroundSprite.getHeight());
		
		playerSprite = game.gfx_atlas.createSprite("doge");
		playerSprite.setSize(1f, 1f);
		playerSprite.setOriginCenter();

		jetpackSprite = game.gfx_atlas.createSprite("jetpack");
		jetpackSprite.setSize(0.5f, 1f);
		jetpackSprite.setOriginCenter();
		
		// for animations
		rocketfuelAnimationTexture = new Texture(Gdx.files.internal("data/img/spriteanims/rocketfuel.png"));
		dogecoinAnimationTexture = new Texture(Gdx.files.internal("data/img/spriteanims/dogecoinanimation.png"));
		bitcoinAnimationTexture = new Texture(Gdx.files.internal("data/img/spriteanims/bitcoinanimation.png"));
		litecoinAnimationTexture = new Texture(Gdx.files.internal("data/img/spriteanims/litecoinanimation.png"));
		explosionAnimationTexture = new Texture(Gdx.files.internal("data/img/spriteanims/explosion.png"));
		
		// gfx animation list
		gfxAnimationList = new ArrayList<GfxAnimation>();
		
		// Fonts, size depending on screen high

//		if (fScaleFactor < 2) {
		// TODO, FIX THIS! Too big on smartphone
		if (fScaleFactor < 2) {
			dogeFont = new BitmapFont(Gdx.files.internal("data/fonts/dogefont32.fnt"));
			dogeFont.setScale(1f/fTiledMapScaleFactor);
		}
		else {
			dogeFont = new BitmapFont(Gdx.files.internal("data/fonts/dogefont64.fnt"));
			dogeFont.setScale((1f/fTiledMapScaleFactor) / 2);
		}
		dogeFont.getRegion().getTexture().setFilter(game.optionsManager.getGraphicsParams(), game.optionsManager.getGraphicsParams());
		dogeFont.setColor(Color.WHITE);
		dogeFont.setUseIntegerPositions(false);
//		}
//		else {
//			dogeFont = new BitmapFont(Gdx.files.internal("data/fonts/dogefont64.fnt"));
//			dogeFont.setScale(0.025f);
//		}
		
		// Tiled map
		TmxMapLoader.Parameters tiledMapParams = new TmxMapLoader.Parameters();
		tiledMapParams.textureMinFilter = game.optionsManager.getGraphicsParams();
		tiledMapParams.textureMagFilter = game.optionsManager.getGraphicsParams();
		
		tiledMap = new TmxMapLoader().load("data/tiledmap/level" + iLevel + ".tmx", tiledMapParams);
		tmr = new OrthogonalTiledMapRenderer(tiledMap, 1f / fTiledMapScaleFactor);
		// only use shader if graphics high
		if (game.optionsManager.isGraphicsHigh())
			tmr.getSpriteBatch().setShader(worldShaderManager.getShaderProgram());
		
		// Box2d Stuff
		box2dDebugRenderer = new Box2DDebugRenderer();
		
		world = new World(new Vector2(0, -9.81f), true);
		world.setContactListener(new ContactHandle(game, this));
		
		// Box2D Stuff for Tiledmap
		createBox2DEntitiesFromTiledMap();
		
		// Player Stuff
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set(20f, 8.5f);
		
		PolygonShape playerShape = new PolygonShape();
		playerShape.setAsBox(0.5f, 0.5f);
		
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = playerShape;
		fixtureDef.density = 2f;
		fixtureDef.friction = 0.0f;
		fixtureDef.restitution = 0.0f;
		
		playerBody = world.createBody(bodyDef);
		playerBody.createFixture(fixtureDef).setUserData("player");
		
		playerLastLocation = new Vector2();
		
		playerLastLocation.x = playerBody.getPosition().x;
		playerLastLocation.y = playerBody.getPosition().y;
		playerBody.setAngularDamping(1f);
		
		playerShape.dispose();
		
		playerBody.setUserData(new DogeData(playerBody, "player", true, playerSprite));
		
		// Jetpack
		bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set(21f, 8.5f);
		
		PolygonShape jetpackShape = new PolygonShape();
		jetpackShape.setAsBox(0.25f, 0.5f);
		
		fixtureDef = new FixtureDef();
		fixtureDef.shape = jetpackShape;
		fixtureDef.density = 2f;
		fixtureDef.friction = 0.0f;
		fixtureDef.restitution = 0.0f;
		
		jetpackBody = world.createBody(bodyDef);
		jetpackBody.createFixture(fixtureDef).setUserData("jetpack");
		
		jetpackBody.setAngularDamping(1f);
		
		jetpackShape.dispose();
		
		jetpackBody.setUserData(new DogeData(jetpackBody, "jetpack", true, jetpackSprite));
		
		WeldJointDef jointDef = new WeldJointDef();
		jointDef.bodyA = playerBody;
		jointDef.bodyB = jetpackBody;
		jointDef.type = JointType.WeldJoint;
		jointDef.localAnchorA.x = 0.5f;
		jointDef.localAnchorA.y = 0;
		jointDef.localAnchorB.x = -0.25f;
		jointDef.localAnchorB.y = 0;
		jointDef.collideConnected = false;
		
		playerJoint = world.createJoint(jointDef);
		playerJoint.setUserData(new Boolean(true)); // just to determine if joint is intact, to not play shaderstuff and hitstuff is jetpack is lost (after game)
		
		// Game Logic pre init
		// Get fuel attribute from map, if map has that attribute
		float fJetpackStartFuel = 100f;
		if (tiledMap.getProperties().containsKey("startfuel"))
			fJetpackStartFuel = Float.valueOf((String) tiledMap.getProperties().get("startfuel"));
		
		jetpack = new Jetpack(game, this, jetpackBody, fJetpackStartFuel);
		player = new Player(game, this, playerBody);
		
		jetpackAnimation = new JetpackAnimation(jetpack, jetpackSprite, rocketfuelAnimationTexture);
		
		// Does the level contain the moon? If so set in Levelmanager options
		if (tiledMap.getProperties().containsKey("moon"))
			levelManager.setMoonLevel();
		
		// Scene2D Stuff - Overlay
		labelCamera = new Label("" + iCameraScaleFactor, game.skin);
		labelCamera.setFontScale(1.5f);
		labelFPS = new Label("" + Gdx.graphics.getFramesPerSecond(), game.skin);
		labelFPS.setFontScale(1.5f);
		labelXSpeed = new Label("", game.skin);
		labelXSpeed.setFontScale(1.5f);
		labelYSpeed = new Label("", game.skin);
		labelYSpeed.setFontScale(1.5f);
		labelAngleSpeed = new Label("", game.skin);
		labelAngleSpeed.setFontScale(1.5f);
		labelStat1 = new Label("fScaleFactor: " + fScaleFactor, game.skin);
		labelStat1.setFontScale(1.5f);
		labelStat2 = new Label("", game.skin);
		labelStat2.setFontScale(1.5f);
		labelStat3 = new Label("", game.skin);
		labelStat3.setFontScale(1.5f);
		
		tableDebug = new Table();
		tableDebug.setFillParent(true);
		tableDebug.pad(0);
		tableDebug.top().left();
		
		tableDebug.add(labelCamera).align(Align.left);
		tableDebug.row();
		tableDebug.add(labelFPS).align(Align.left);
		tableDebug.row();
		tableDebug.add(labelXSpeed).align(Align.left);
		tableDebug.row();
		tableDebug.add(labelYSpeed).align(Align.left);
		tableDebug.row();
		tableDebug.add(labelAngleSpeed).align(Align.left);
		tableDebug.row();
		tableDebug.add(labelStat1).align(Align.left);
		tableDebug.row();
		tableDebug.add(labelStat2).align(Align.left);
		tableDebug.row();
		tableDebug.add(labelStat3).align(Align.left);
		
		tableDebug.setVisible(false);
		
		if (fScaleFactor < 2) {
			labelTimePassed = new Label("", game.skin, "dogeinfo32");
			labelDogeInfo = new Label("", game.skin, "dogeinfo32");
			labelBitInfo = new Label("", game.skin, "dogeinfo32");
			labelLiteInfo = new Label("", game.skin, "dogeinfo32");
			labelJetpackLifeInfo = new Label("", game.skin, "dogeinfo32");
			labelPlayerLifeInfo = new Label("", game.skin, "dogeinfo32");
			labelFuelInfo = new Label("", game.skin, "dogeinfo32");
		}
		else {
			labelTimePassed = new Label("", game.skin, "dogeinfo64");
			labelDogeInfo = new Label("", game.skin, "dogeinfo64");
			labelBitInfo = new Label("", game.skin, "dogeinfo64");
			labelLiteInfo = new Label("", game.skin, "dogeinfo64");
			labelJetpackLifeInfo = new Label("", game.skin, "dogeinfo64");
			labelPlayerLifeInfo = new Label("", game.skin, "dogeinfo64");
			labelFuelInfo = new Label("", game.skin, "dogeinfo64");
		}
		
		final float fXSpaceBetweenButtons = Gdx.graphics.getWidth() / 100f;
		final float fYButtonPosY = Gdx.graphics.getHeight() / 10f;
		
		float gasButtonSize = Gdx.graphics.getWidth() / 4f;
		gasButton = new TextButton("", game.skin, "gas_button");
		gasButton.setWidth(gasButtonSize);
		gasButton.setHeight(gasButtonSize);
		gasButton.setPosition(Gdx.graphics.getWidth() - gasButtonSize, 0);
		
		float sideControlsButtonSize = Gdx.graphics.getWidth() / 6f;
		leftButton = new TextButton("", game.skin, "left_button");
		leftButton.setWidth(sideControlsButtonSize);
		leftButton.setHeight(sideControlsButtonSize);
		leftButton.setPosition(fXSpaceBetweenButtons, fYButtonPosY);
		
		rightButton = new TextButton("", game.skin, "right_button");
		rightButton.setWidth(sideControlsButtonSize);
		rightButton.setHeight(sideControlsButtonSize);
		rightButton.setPosition(sideControlsButtonSize + 3*fXSpaceBetweenButtons, fYButtonPosY);
		
		tankInner = new Image(game.skin.getDrawable("tankinner"));
		Image tankOuter = new Image(game.skin.getDrawable("tankouter"));
		
		float fTankScaleFactor = tankOuter.getHeight() / gasButtonSize;
		tankInner.setHeight(tankInner.getHeight() / fTankScaleFactor);
		tankInner.setWidth(tankInner.getWidth() / fTankScaleFactor);
		tankInner.setPosition(Gdx.graphics.getWidth() - gasButtonSize - tankInner.getWidth() - 6, 0);
		tankOuter.setHeight(tankOuter.getHeight() / fTankScaleFactor);
		tankOuter.setWidth(tankOuter.getWidth() / fTankScaleFactor);
		tankOuter.setPosition(Gdx.graphics.getWidth() - gasButtonSize - tankOuter.getWidth() - 6, 0);
		
		fMaxTankHeight = tankOuter.getHeight();
		
		TextButton jetpackLifeImage = new TextButton("", game.skin, "jetpacklife");
		TextButton playerLifeImage = new TextButton("", game.skin, "dogelife");
		
		Table tableBottomInfo = new Table();
		tableBottomInfo.setFillParent(true);
		tableBottomInfo.bottom();
		if (fScaleFactor < 2f)
			tableBottomInfo.add(playerLifeImage).width(48).height(48);
		else
			tableBottomInfo.add(playerLifeImage).width(96).height(96);
		tableBottomInfo.add(labelPlayerLifeInfo).padLeft(8);
		tableBottomInfo.row();
		if (fScaleFactor < 2f)
			tableBottomInfo.add(jetpackLifeImage).width(48).height(48);
		else
			tableBottomInfo.add(jetpackLifeImage).width(96).height(96);
		tableBottomInfo.add(labelJetpackLifeInfo).padLeft(8);
//		tableBottomInfo.debug();
		
		if (fScaleFactor < 2)
			countdownOverlay = new TextButton("", game.skin, "halftransp32");
		else
			countdownOverlay = new TextButton("", game.skin, "halftransp64");
		countdownOverlay.setWidth(Gdx.graphics.getWidth());
		countdownOverlay.setHeight(Gdx.graphics.getHeight()/8);
		countdownOverlay.setPosition(0, (Gdx.graphics.getHeight() / 2 - countdownOverlay.getHeight() / 2) + Gdx.graphics.getHeight()/6);
		
		if (fScaleFactor < 2)
			lossContinueButton = new TextButton("", game.skin, "halftransp32");
		else
			lossContinueButton = new TextButton("", game.skin, "halftransp64");
		lossContinueButton.setWidth(Gdx.graphics.getWidth());
		lossContinueButton.setHeight(Gdx.graphics.getHeight()/4);
		lossContinueButton.setPosition(0, (Gdx.graphics.getHeight() / 2 - lossContinueButton.getHeight() / 2) + Gdx.graphics.getHeight()/6);
		
		tableUIInfo = new Table();
		tableUIInfo.pad(0);
		tableUIInfo.padRight(12);
		tableUIInfo.setFillParent(true);
		tableUIInfo.top().right();
		tableUIInfo.add(labelTimePassed).align(Align.right);
		tableUIInfo.row();
		tableUIInfo.add(labelDogeInfo).align(Align.right);
		tableUIInfo.row();
		tableUIInfo.add(labelBitInfo).align(Align.right);
		tableUIInfo.row();
		tableUIInfo.add(labelLiteInfo).align(Align.right);
		
		stage.addActor(tableDebug);
		stage.addActor(tableUIInfo);
		stage.addActor(gasButton);
		stage.addActor(leftButton);
		stage.addActor(rightButton);
		stage.addActor(tankInner);
		stage.addActor(tankOuter);
		stage.addActor(tableBottomInfo);
		stage.addActor(countdownOverlay);
		
		game.getRequestHandler().showAds(false);
		
		if (levelManager.isMoonlevel())
			game.getMusicManager().setMusic_Game(MusicType.ATLAST);
		else {
			game.getMusicManager().setStopMusicRequest();
		}
		
		countdown = new Countdown(3);
		dogeEmote = new DogeEmote(batch, dogeFont);
	}

	@Override
	public void hide() {
		this.dispose();
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		Entity.minionList.clear();
		Dolan.dolanList.clear();
		box2dDebugRenderer.dispose();
		world.dispose();
		tiledMap.dispose();
		tmr.dispose();
		gameBackground.dispose();
		explosionAnimationTexture.dispose();
		dogecoinAnimationTexture.dispose();
		bitcoinAnimationTexture.dispose();
		litecoinAnimationTexture.dispose();
		batch.dispose();
		stage.dispose();
	}
	
	private void createBox2DEntitiesFromTiledMap () {
		System.out.println("Trying to create box2d objects from tiled map");
		BodyDef bodyDef = new BodyDef();
		Shape shape = null;
		
		float width = 0; // default init
		float height = 0; // default init
		
		for (MapObject object : tiledMap.getLayers().get("Box2D").getObjects()) {
			// to test if flags can be applied
			boolean bLegitObject = false;
			Body body = null;
			
			if (object instanceof RectangleMapObject) {
				shape = new PolygonShape();
				
				Rectangle rect = ((RectangleMapObject) object).getRectangle();
				
				// save width and height
				width = rect.width / fTiledMapScaleFactor;
				height = rect.height / fTiledMapScaleFactor;
				
				((PolygonShape) shape).setAsBox((rect.width / 2) / fTiledMapScaleFactor, (rect.height / 2) / fTiledMapScaleFactor);
				
				bodyDef.position.x = (rect.x / fTiledMapScaleFactor) + ((rect.width / fTiledMapScaleFactor) / 2);
				bodyDef.position.y = (rect.y / fTiledMapScaleFactor) + ((rect.height / fTiledMapScaleFactor) / 2);
				
				bLegitObject = true;
			}
			else if (object instanceof EllipseMapObject) {
				System.out.println("Found a circle!");
				
				shape = new CircleShape();
				
				Ellipse ellipse = ((EllipseMapObject) object).getEllipse();
				
				// save width and height - only circles supported?
				width = height = ellipse.width / fTiledMapScaleFactor;
				
				((CircleShape) shape).setRadius(width / 2f);
				
				bodyDef.position.x = (ellipse.x / fTiledMapScaleFactor) + ((ellipse.width / fTiledMapScaleFactor) / 2);
				bodyDef.position.y = (ellipse.y / fTiledMapScaleFactor) + ((ellipse.height / fTiledMapScaleFactor) / 2);
				
				bLegitObject = true;
			}
//			else if (object instanceof PolygonMapObject) {
//				shape = new PolygonShape();
//				
//				Polygon poly = ((PolygonMapObject) object).getPolygon();
//				
//				float vertices[] = poly.getTransformedVertices();
//				
//				for (int i = 0; i < vertices.length; i++) {
//					vertices[i] /= fTiledMapScaleFactor;
//				}
//				
//				System.out.println("Found PolygonMapObject, vertices-count: " + vertices.length);
//				
//				((PolygonShape) shape).set(vertices);
//				
//				bodyDef.position.x = poly.getOriginX() / fTiledMapScaleFactor;
//				bodyDef.position.y = poly.getOriginY() / fTiledMapScaleFactor;
//				
//				bLegitObject = true;
//			}
			else {
				System.out.println("CRITICAL! No fetchable object, object type was: " + object.getName() + ", " + object.getClass());
			}
			
			if (bLegitObject) {
				Sprite objectSprite = null;
				
				// pre body creation stuff
				if (object.getProperties().containsKey("bodyType")) {
					if (object.getProperties().get("bodyType").equals("DynamicBody"))
						bodyDef.type = BodyType.DynamicBody;
					else if (object.getProperties().get("bodyType").equals("KinematicBody"))
						bodyDef.type = BodyType.KinematicBody;
					else if (object.getProperties().get("bodyType").equals("StaticBody")) {
						bodyDef.type = BodyType.StaticBody;
						bodyDef.awake = false;
					}
					else {
						System.out.println("Found parameter bodyType, but no valid description! Description was: " + object.getProperties().get("bodyType"));
						bodyDef.type = BodyType.StaticBody;
						bodyDef.awake = false;
					}
				}
				else { // Default body type
					bodyDef.type = BodyType.StaticBody;
				}
				
				// create body
				body = world.createBody(bodyDef);
				body.createFixture(shape, 1f);
				
				// post body creation
				if (object.getProperties().containsKey("solid")) {
					if (object.getProperties().get("solid").equals("false"))
						body.getFixtureList().get(0).setSensor(true);
					else if (object.getProperties().get("solid").equals("true"))
						;
					else {
						System.out.println("Found parameter solid, but no valid description! Description was: " + object.getProperties().get("solid"));
					}
				}
				else
					; // do nothing
				
				if (object.getProperties().containsKey("sprite")) {
					if (object instanceof RectangleMapObject || object instanceof EllipseMapObject) {
						objectSprite = game.gfx_atlas.createSprite((String) object.getProperties().get("sprite"));
						
						objectSprite.setSize(width, height);
						objectSprite.setOriginCenter();
					}
				}
				
				if (object.getProperties().containsKey("sType")) {
					// if dogecoin, litecoin or bitcoin use animation instead of sprite
					if (object.getProperties().get("sType").equals("dogecoin") || object.getProperties().get("sType").equals("litecoin") || object.getProperties().get("sType").equals("bitcoin")) {
						if (object.getProperties().get("sType").equals("dogecoin"))
							body.setUserData(new DogeData(body, (String) object.getProperties().get("sType"), body.getFixtureList().get(0).isSensor(), dogecoinAnimationTexture, width, height, 29, 1));
						else if (object.getProperties().get("sType").equals("bitcoin"))
							body.setUserData(new DogeData(body, (String) object.getProperties().get("sType"), body.getFixtureList().get(0).isSensor(), bitcoinAnimationTexture, width, height, 29, 1));
						else if (object.getProperties().get("sType").equals("litecoin"))
							body.setUserData(new DogeData(body, (String) object.getProperties().get("sType"), body.getFixtureList().get(0).isSensor(), litecoinAnimationTexture, width, height, 29, 1));
					}
					else if (objectSprite == null)
						body.setUserData(new DogeData(body, (String) object.getProperties().get("sType"), body.getFixtureList().get(0).isSensor()));
					else
						body.setUserData(new DogeData(body, (String) object.getProperties().get("sType"), body.getFixtureList().get(0).isSensor(), objectSprite));
					
					// Add enemies
					if (object.getProperties().containsKey("enemy")) {
						// only groundcat and flycat have direction
						EntityDirection startDirection = null;
						if (object.getProperties().containsKey("direction")) {
							if (object.getProperties().get("direction").equals("right")) {
								startDirection = EntityDirection.RIGHT;
							}
							else if (object.getProperties().get("direction").equals("left")) {
								startDirection = EntityDirection.LEFT;
							}
							else if (object.getProperties().get("direction").equals("top")) {
								startDirection = EntityDirection.TOP;
							}
							else if (object.getProperties().get("direction").equals("bottom")) {
								startDirection = EntityDirection.BOTTOM;
							}
							else { // is there, but wrongly set, sysout and right
								System.out.println("Found parameter direction, but no valid description! Description was: " + object.getProperties().get("direction"));
								startDirection = EntityDirection.RIGHT;
							}
						}
						else { // no direction paramter, default to right
							// can be zero for dolan
							// System.out.println("Parameter direction not set for enemy " + object.getProperties().get("sType"));
							startDirection = EntityDirection.RIGHT;
						}
						
						// movable coins
						if (object.getProperties().get("sType").equals("mdogecoin") || object.getProperties().get("sType").equals("mbitcoin") || object.getProperties().get("sType").equals("mlitecoin")) {
							if (object.getProperties().containsKey("vertical"))
								Entity.minionList.add(new MoveableCoin(body, startDirection, true));
							else
								Entity.minionList.add(new MoveableCoin(body, startDirection, false));
							
							DogeData dd = (DogeData) body.getUserData();
							dd.setMovableCoin();
						}
						
						// regular enemies
						else if (object.getProperties().get("sType").equals("groundcat"))
							Entity.minionList.add(new Groundcat(body, startDirection));
						
						else if (object.getProperties().get("sType").equals("flycat")) {
							if (object.getProperties().containsKey("vertical"))
								Entity.minionList.add(new Flycat(body, startDirection, true));
							else
								Entity.minionList.add(new Flycat(body, startDirection, false));
						}
						
						else if (object.getProperties().get("sType").equals("dolan"))
							Entity.minionList.add(new Dolan(this, body));
						
						else if (object.getProperties().get("sType").equals("saw"))
							Entity.minionList.add(new BuzzSaw(this, body));
						
						// Error if groundcat or flycat and direction not set !
					}
				}
				else {
					// no sType set = environment
					body.setUserData(new DogeData(body, "environment", body.getFixtureList().get(0).isSensor()));
				}
			}
		}
		if (shape != null)
			shape.dispose();
	}
	
	public void toggleCameraLock() {
		cameralocked = cameralocked ? false : true;
		System.out.println(cameralocked);
	}
	
	public Table getTableOverlay() {
		return tableDebug;
	}
	
	private void fadeOutOverlay() {
		AlphaAction fadeOutAction = new AlphaAction();
		fadeOutAction.setDuration(1f);
		fadeOutAction.setAlpha(0f);
		
		countdownOverlay.addAction(fadeOutAction);
		
		System.out.println("Fade out action called!");
	}
	
	public OrthographicCamera getCamera() {
		return camera;
	}
	
	public int getCameraScaleFactor() {
		return iCameraScaleFactor;
	}
	
	public Player getPlayer() {
		return player;
	}

	public Jetpack getJetpack() {
		return jetpack;
	}
	
	public void setCameraScaleFactor(int iScale) {
		iCameraScaleFactor = iScale;
		if (iCameraScaleFactor < 2)
			iCameraScaleFactor = 1;
		System.out.println(iCameraScaleFactor);
		camera.viewportWidth = Gdx.graphics.getWidth() / iCameraScaleFactor;
		camera.viewportHeight = Gdx.graphics.getHeight() / iCameraScaleFactor;
		camera.update();
	}
	
	public void showDefeatPopup(DeathReason reason) {
		if (!bDefeatPopupShown) {
			switch(reason) {
			case DOGEDEAD:
				lossContinueButton.setText("Doge got injured and needs to go to the hospital" + "\n\nClick to continue");
				break;
			case JETPACKDEAD:
				lossContinueButton.setText("CRITICAL MISSION FAILURE! Jetpack went to shit" + "\n\nClick to continue");
				break;
			case DESERTION:
				lossContinueButton.setText("Doge deserted by trying to get back to earth, this is no option!" + "\n\nClick to continue");
				break;
			case NOFUEL:
				lossContinueButton.setText("Jetpack is out of fuel, Doge is lost in space" + "\n\nClick to continue");
			default:
				assert false;
				break;
			}
			stage.addActor(lossContinueButton);
			stage.addListener(new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {
					levelManager.setReadyForDefeatScreen();
				}
			});
			bDefeatPopupShown = true;
		}
	}
	
	public TextButton getLeftButton() {
		return leftButton;
	}
	
	public TextButton getRightButton() {
		return rightButton;
	}
	
	public TextButton getGasButton() {
		return gasButton;
	}
	
	public void addAnimation(GfxAnimation animation) {
		gfxAnimationList.add(animation);
	}
	
	public ShaderManager getWorldShaderManager() {
		return worldShaderManager;
	}
	
	public World getWorld() {
		return world;
	}
	
	public Joint getPlayerJoint() {
		return playerJoint;
	}
}

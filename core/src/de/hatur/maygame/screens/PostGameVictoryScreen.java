package de.hatur.maygame.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import de.hatur.maygame.MayGame;
import de.hatur.maygame.handles.LevelManager;

public class PostGameVictoryScreen implements Screen {
	
	private MayGame game;
	private LevelManager levelManager;
	
	private Stage stage;
	private Table tablePostGameBackground;
	private Table tablePostGame;
	
	public PostGameVictoryScreen(MayGame game, LevelManager levelManager) {
		this.game = game;
		this.levelManager = levelManager;
	}

	@Override
	public void render(float delta) {
		Gdx.graphics.getGL20().glClearColor(0, 0, 0, 1);
		Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		
		stage.act();
		stage.draw();
		
		Table.drawDebug(stage);
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show() {
		stage = new Stage(new StretchViewport(MayGame.VIRTUAL_WIDTH, MayGame.VIRTUAL_HEIGHT));
		
		InputMultiplexer multiplexer = new InputMultiplexer(game.backprocessor, stage);
		Gdx.input.setInputProcessor(multiplexer);
		
		createInterface();
		
		game.getRequestHandler().showAds(true);
	}

	@Override
	public void hide() {
		this.dispose();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		stage.dispose();
	}
	
	private void createInterface() {
		tablePostGameBackground = new Table();
		tablePostGame = new Table();
		
		// Calculate and indicate if this is a new record
		boolean bNewRecord = levelManager.calcAndSaveLevelData(game.saveManager);
		
		TextButton tbBack = new TextButton("To main menu", game.skin, "bigdoge");
		tbBack.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(new MainmenuScreen(game));
			}
		});
		
		TextButton tbNextLevel = new TextButton("Next level", game.skin, "bigdoge");
		tbNextLevel.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (levelManager.getCurrentLevel()+1 <= MayGame.AVAILABLE_LEVELS) {
					game.setScreen(new GameScreen(game, levelManager.getCurrentLevel()+1));
				}
			}
		});
		
		TextButton tbRestart = new TextButton("Restart", game.skin, "bigdoge");
		tbRestart.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(new GameScreen(game, levelManager.getCurrentLevel()));
			}
		});
		
		Label labelRecordOrNot;
		Label labelPercentageTime;
		
		if (bNewRecord) {
			labelRecordOrNot = new Label("Wow, new personal record, very proud!", game.skin, "dogeinfo22");
			labelPercentageTime = new Label(levelManager.getLevelPercentage() + "% in " + levelManager.getTimePassed(), game.skin, "dogeinfo32");
		}
		else {
			labelRecordOrNot = new Label("So victory, but done better before! Much sad!", game.skin, "dogeinfo22");
			labelPercentageTime = new Label((int) levelManager.getLevelPercentage() + "% in " + levelManager.getTimePassed()
					+ " (best: " + (int) game.saveManager.levelRecords.get(levelManager.getCurrentLevel()).getPercentageDone(game.saveManager.levelData.get(levelManager.getCurrentLevel()))
					+ "% in " + game.saveManager.levelRecords.get(levelManager.getCurrentLevel()).getFormattedTime() + ")", game.skin, "dogeinfo32");
		}
		
		HorizontalGroup hgHeadingHack = new HorizontalGroup();
		if (!levelManager.isMoonlevel()) {
			hgHeadingHack.addActor(new Image(game.gfx.getDrawable("stagevictory")));
			hgHeadingHack.addActor(new Label(" VICTORY!", game.skin, "dogeinfo64"));
		}
		else {
			hgHeadingHack.addActor(new Image(game.gfx.getDrawable("victory")));
			hgHeadingHack.addActor(new Label(" DOGE ON MOON!", game.skin, "dogeinfo64"));
		}
		
		double levelPercent = game.saveManager.levelRecords.get(levelManager.getCurrentLevel()).getPercentageDone(game.saveManager.levelData.get(levelManager.getCurrentLevel()));
		boolean nextLevelUnlocked = levelPercent >= 80.0 ? true : false;
		
		// Background table
		tablePostGameBackground.setSize(480, 360);
		tablePostGameBackground.setPosition((MayGame.VIRTUAL_WIDTH / 2) - (tablePostGameBackground.getWidth() / 2), (MayGame.VIRTUAL_HEIGHT / 2) - (tablePostGameBackground.getHeight() / 2)+36);
		tablePostGameBackground.pad(0);
		tablePostGameBackground.top().center();
		tablePostGameBackground.add(hgHeadingHack).colspan(2);
		tablePostGameBackground.row();
		tablePostGameBackground.add().colspan(2).expandY();
		tablePostGameBackground.row();
		if (levelManager.getCurrentLevel() + 1 <= MayGame.AVAILABLE_LEVELS && nextLevelUnlocked) {
			tablePostGameBackground.add(tbBack).width(200).height(80);
			tablePostGameBackground.add(tbNextLevel).width(200).height(80).padLeft(12);
		}
		else {
			tablePostGameBackground.add(tbBack).width(200).height(80); // colspan 2
			tablePostGameBackground.add(tbRestart).width(200).height(80).padLeft(12);
		}
			
		
//		tablePostGameBackground.debug();
		
		tablePostGame.setSize(480, 360);
		tablePostGame.setPosition((MayGame.VIRTUAL_WIDTH / 2) - (tablePostGame.getWidth() / 2), (MayGame.VIRTUAL_HEIGHT / 2) - (tablePostGame.getHeight() / 2)+32);
		tablePostGame.pad(0);
		tablePostGame.top().center();
		tablePostGame.row();
		if (levelManager.getCurrentLevel() != 10)
			tablePostGame.add(new Label("- Level " + levelManager.getCurrentLevel() + " done -", game.skin, "dogeinfo32")).colspan(2).padTop(60);
		else
			tablePostGame.add(new Label("- CONGRATULATIONS, YOU BEAT THE FINAL LEVEL! -", game.skin, "dogeinfo32")).colspan(2).padTop(60);
		tablePostGame.row();
		tablePostGame.add(labelRecordOrNot).colspan(2);
		tablePostGame.row();
		tablePostGame.add(new Label("Statistics:", game.skin, "dogeinfo22")).colspan(2);
		tablePostGame.row();
		tablePostGame.add(labelPercentageTime).colspan(2);
		tablePostGame.row();
		tablePostGame.add(new Label("Collected Dogecoin: " + levelManager.getCollectedDogecoin() + " / " + levelManager.getTotalDogecoin(), game.skin, "dogeinfo22")).colspan(2);
		tablePostGame.row();
		tablePostGame.add(new Label("Collected Bitcoin: " + levelManager.getCollectedBitcoin() + " / ?", game.skin, "dogeinfo22")).colspan(2);
		tablePostGame.row();
		tablePostGame.add(new Label("Collected Litecoin: " + levelManager.getCollectedLitecoin() + " / ?", game.skin, "dogeinfo22")).colspan(2);
		tablePostGame.row();
		tablePostGame.add().colspan(2).expandY().padBottom(82);
		
//		tablePostGame.debug();
		
		stage.addActor(tablePostGameBackground);
		stage.addActor(tablePostGame);
	}
}

package de.hatur.maygame.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import de.hatur.maygame.MayGame;
import de.hatur.maygame.particle.Particle.StandardType;
import de.hatur.maygame.particle.ParticleManager;

public class LevelSelectionScreen implements Screen {
	private MayGame game;
	
	private Stage stage;
	
	private Group backgroundGroup;
	private Table tableLevelBackground;
	private Table tableLevelList;
	
	private ParticleManager particleManager;
	
	public LevelSelectionScreen(MayGame game) {
		this.game = game;
	}

	@Override
	public void render(float delta) {
		Gdx.graphics.getGL20().glClearColor(0, 0, 0, 1);
		Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		
		stage.act();
		stage.draw();
		
		if (particleManager.isStarted())
			particleManager.manage(delta, 1.00f, game.gfx.getDrawable("dogecoin"), (int) (Math.random() * 5 + 4), StandardType.TOP_BOTTOM, null, null);
		
		// To avoid delta fluctuations from first delta (long one) in first frame
		if (!particleManager.isStarted())
			particleManager.start();
		
//		Table.drawDebug(stage);
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show() {
		stage = new Stage(new StretchViewport(MayGame.VIRTUAL_WIDTH, MayGame.VIRTUAL_HEIGHT));
		
		InputMultiplexer multiplexer = new InputMultiplexer(game.backprocessor, stage);
		Gdx.input.setInputProcessor(multiplexer);
		
		backgroundGroup = new Group();
		particleManager = new ParticleManager(backgroundGroup);
		
		createInterface();
		
		game.getRequestHandler().showAds(true);
	}

	@Override
	public void hide() {
		this.dispose();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		particleManager.dispose();
		stage.dispose();
	}
	
	private void createInterface() {
		tableLevelBackground = new Table();
		tableLevelList = new Table();
		
		TextButton tbBack = new TextButton("BACK", game.skin, "bigdoge");
		tbBack.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.setScreen(new MainmenuScreen(game));
			}
		});
		
		tableLevelBackground.setSize(480, 360);
		tableLevelBackground.setPosition((MayGame.VIRTUAL_WIDTH / 2) - (tableLevelBackground.getWidth() / 2), (MayGame.VIRTUAL_HEIGHT / 2) - (tableLevelBackground.getHeight() / 2)+20);
		tableLevelBackground.pad(0);
		tableLevelBackground.top().center();
		tableLevelBackground.add(new Image(game.gfx.getDrawable("dogecoin"))).padRight(16);
		tableLevelBackground.add(new Label("Level selection", game.skin, "dogeinfo32")).align(Align.left);
		tableLevelBackground.row().padTop(6);
		tableLevelBackground.add().colspan(2).expandY();
		tableLevelBackground.row().padTop(6);
		tableLevelBackground.add(tbBack).colspan(2).width(200).height(60).padBottom(24);
		
//		tableLevelBackground.debug();
		
		tableLevelList.setSize(480, 360);
		tableLevelList.setPosition((MayGame.VIRTUAL_WIDTH / 2) - (tableLevelBackground.getWidth() / 2), (MayGame.VIRTUAL_HEIGHT / 2) - (tableLevelBackground.getHeight() / 2)+28);
		tableLevelList.pad(0);
		tableLevelList.top().center();
		
		// Create level click buttons with infos
		
		game.saveManager.loadLevelRecords();
		
		for (int i = 1; i <= 10; i++) {
			boolean bIsInLevelRange = false;
			
			if (i <= MayGame.AVAILABLE_LEVELS)
				bIsInLevelRange = true;
			
			Table tableSingleLevelTable = new Table();
			
			TextButton tbEnterLevel = null;
			Label labelPercent = null;
			Label labelScore = null;
			
			if (bIsInLevelRange) {
				if (game.saveManager.levelRecords.get(i).isUnlocked()) {
					tbEnterLevel = new TextButton("" + i, game.skin, "bigdoge");
					labelPercent = new Label("Best: " + (int) game.saveManager.levelRecords.get(i).getPercentageDone(game.saveManager.levelData.get(i)) + "%" , game.skin, "dogeinfo22");
					labelScore = new Label("in " + game.saveManager.levelRecords.get(i).getFormattedTime(), game.skin, "dogeinfo22");
				}
				else if (game.saveManager.levelRecords.get(i-1).isUnlocked()) {
					tbEnterLevel = new TextButton("" + i, game.skin, "bigdoge_disabled");
					labelPercent = new Label("80% in Level " + (i-1), game.skin, "dogeinfo22");
					labelScore = new Label("to unlock!", game.skin, "dogeinfo22");
				}
				else {
					tbEnterLevel = new TextButton("" + i, game.skin, "bigdoge_disabled");
					labelPercent = new Label(" ", game.skin, "dogeinfo22");
					labelScore = new Label(" ", game.skin, "dogeinfo22");
				}
			}
			else {
				tbEnterLevel = new TextButton("", game.skin, "bigdoge_disabled");
				labelPercent = new Label(" ", game.skin, "dogeinfo22");
				labelScore = new Label(" ", game.skin, "dogeinfo22");
			}
			
			tableSingleLevelTable.add(tbEnterLevel).width(100).height(50).padLeft(8).padRight(8);
			tableSingleLevelTable.row();
			tableSingleLevelTable.add(labelPercent).padLeft(8).padRight(8).padTop(-2);
			tableSingleLevelTable.row();
			tableSingleLevelTable.add(labelScore).padLeft(8).padRight(8).padTop(-4);
			tableLevelList.add(tableSingleLevelTable);
			
			final int index = i;
			
			if (bIsInLevelRange && game.saveManager.levelRecords.get(i).isUnlocked())
				tableSingleLevelTable.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						game.setScreen(new GameScreen(game, index));
					}
				});
			
			if (i % 5 == 0)
				tableLevelList.row().padTop(4);
		}
		
//		tableLevelList.debug();
		
		stage.addActor(backgroundGroup);
		stage.addActor(tableLevelBackground);
		stage.addActor(tableLevelList);
	}

}

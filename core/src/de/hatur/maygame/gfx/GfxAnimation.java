package de.hatur.maygame.gfx;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;

public class GfxAnimation {
	private Animation animation;
	private TextureRegion[] trFrames;
	private float textureWidth = 0;
	private float textureHeight = 0;
	private float posX;
	private float posY;
	private Body body;
	private float offsetX = 0.0f;
	private float offsetY = 0.0f;
	private int iNumKeyframes;
	private float fStateTime = 0;
	
	/**
	 * For one time animations at a fixed place
	 * @param texture
	 * @param posX
	 * @param posY
	 * @param width
	 * @param height
	 * @param duration
	 * @param cells
	 * @param rows
	 */
	public GfxAnimation(Texture texture, float posX, float posY, float width, float height, float duration, int cells, int rows) {
		TextureRegion[][] trTmp = TextureRegion.split(texture, texture.getWidth() / cells, texture.getHeight() / rows);
		trFrames = new TextureRegion[cells * rows];
		
		int index = 0;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cells; j++) {
				trFrames[index++] = trTmp[i][j];
			}
		}
		
		iNumKeyframes = cells * rows;
		animation = new Animation(1f/(iNumKeyframes/duration), trFrames);
		textureWidth = width;
		textureHeight = height;
	
		this.posX = posX;
		this.posY = posY;
	}
	
	/**
	 * For one time animations on a moveable place (body)
	 * @param texture
	 * @param body
	 * @param width
	 * @param height
	 * @param duration
	 * @param cells
	 * @param rows
	 */
	public GfxAnimation(Texture texture, Body body, float width, float height, float duration, int cells, int rows) {
		TextureRegion[][] trTmp = TextureRegion.split(texture, texture.getWidth() / cells, texture.getHeight() / rows);
		trFrames = new TextureRegion[cells * rows];
		
		int index = 0;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cells; j++) {
				trFrames[index++] = trTmp[i][j];
			}
		}
		
		iNumKeyframes = cells * rows;
		animation = new Animation(1f/(iNumKeyframes/duration), trFrames);
		textureWidth = width;
		textureHeight = height;
		
		this.body = body;
	}
	
	/**
	 * For one time animations on a moveable place (body) + OFFSET (Verschiebung)
	 * @param texture
	 * @param body
	 * @param offsetX
	 * @param offsetY
	 * @param width
	 * @param height
	 * @param duration
	 * @param cells
	 * @param rows
	 */
	public GfxAnimation(Texture texture, Body body, float offsetX, float offsetY, float width, float height, float duration, int cells, int rows) {
		TextureRegion[][] trTmp = TextureRegion.split(texture, texture.getWidth() / cells, texture.getHeight() / rows);
		trFrames = new TextureRegion[cells * rows];
		
		int index = 0;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cells; j++) {
				trFrames[index++] = trTmp[i][j];
			}
		}
		
		iNumKeyframes = cells * rows;
		animation = new Animation(1f/(iNumKeyframes/duration), trFrames);
		textureWidth = width;
		textureHeight = height;
		
		this.body = body;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
	}
	
	/**
	 * returns the current keyframe and adds to statetime
	 * @param delta
	 * @return
	 */
	public Sprite getKeyFrame(float delta) {
		fStateTime += delta;
		Sprite returnSprite = new Sprite(animation.getKeyFrame(fStateTime));
		returnSprite.setSize(textureWidth, textureHeight);
		if (body != null) {
			float x = body.getPosition().x + offsetX;
			float y = body.getPosition().y + offsetY;
			returnSprite.setPosition(x - returnSprite.getWidth() / 2, y - returnSprite.getHeight() / 2);
		}
		else
			returnSprite.setPosition(posX, posY);
		isActive();
		return returnSprite;
	}
	
	/**
	 * is Animation playing?
	 * @return
	 */
	public boolean isActive() {
		return fStateTime > iNumKeyframes * animation.frameDuration ? false : true;
	}
}

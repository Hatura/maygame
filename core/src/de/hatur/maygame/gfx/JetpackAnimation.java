package de.hatur.maygame.gfx;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;

import de.hatur.maygame.handles.Jetpack;

public class JetpackAnimation {
	private Jetpack jetpack;
	private Sprite jetpackSprite;
	
	private enum AnimationState {
		STARTING,
		RUNNING,
		COOLING
	};
	
	// for animation
	private Animation animation;
	private TextureRegion[] frames;
	private int numKeyframes;
	private float statetime = 0;
	
	private AnimationState currentState;

	public JetpackAnimation(Jetpack jetpack, Sprite jetpackSprite, Texture texture) {
		this.jetpack = jetpack;
		this.jetpackSprite = jetpackSprite;
		
		TextureRegion[][] trTmp = TextureRegion.split(texture, 48, 64);
		frames = new TextureRegion[14];
		
		int index = 0;
		for (int i = 0; i < 1; i++) {
			for (int j = 0; j < 14; j++) {
				frames[index++] = trTmp[i][j];
			}
		}
		
		numKeyframes = 8;
		
		animation = new Animation((1f/14f)/2f, frames); // half speed
		
		currentState = AnimationState.STARTING;
	}
	
	public Sprite getFrame(float delta) {
		if (statetime > 0.5f && jetpack.isFueled())
			statetime = 0.26f;
		
		if (jetpack.isFueled() && statetime <= 0.25f)
			currentState = AnimationState.STARTING;
		else if (jetpack.isFueled() && statetime > 0.25f)
			currentState = AnimationState.RUNNING;
		else if (jetpack.isSideFueled() && !jetpack.isFueled()) {
			currentState = AnimationState.RUNNING;
			statetime -= delta;
			if (statetime < 1f/14f) {
				statetime = 1f/15f;
			}
		}
		else if (!jetpack.isFueled() && statetime != 0) {
			if (statetime > 0.25f)
				statetime = 0.26f;
			currentState = AnimationState.COOLING;
		}
		
 		Sprite returnSprite = new Sprite(animation.getKeyFrame(statetime, false));
 		
		returnSprite.setSize(1f, 1f);
		returnSprite.setOrigin(returnSprite.getWidth() / 2, returnSprite.getHeight() / 2 + 1f);
		returnSprite.setPosition(jetpack.getJetpackBody().getPosition().x - returnSprite.getWidth() / 2, jetpack.getJetpackBody().getPosition().y - returnSprite.getWidth() / 2 - 1f);
		returnSprite.setRotation(jetpack.getJetpackBody().getAngle() * MathUtils.radiansToDegrees);
		
		if (currentState == AnimationState.STARTING || currentState == AnimationState.RUNNING && jetpack.isFueled())
			statetime += delta;
		else if (currentState == AnimationState.COOLING) {
			statetime -= delta;
			if (statetime < 0) {
				statetime = 0;
				return null;
			}
		}
		
		return returnSprite;
	}
}

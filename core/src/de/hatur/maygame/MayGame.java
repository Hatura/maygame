package de.hatur.maygame;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.hatur.maygame.external.IActivityRequestHandler;
import de.hatur.maygame.handles.DogeCycle;
import de.hatur.maygame.handles.MusicManager;
import de.hatur.maygame.handles.OptionsManager;
import de.hatur.maygame.handles.SoundManager;
import de.hatur.maygame.savemanager.SaveManager;
import de.hatur.maygame.screens.MainmenuScreen;

public class MayGame extends Game {
	
	private IActivityRequestHandler myRequestHandler;
	
	public enum Screen {
		MainmenuScreen
	};
	
	// Some game Vars
	public static final String GAME_NAME = "DOGEQUEST";
	public static final String GAME_VERSION = "0.4.1";
	public static final String GAME_VERSION_DATE = "29.05.2014 00:17";
	
	public static final int AVAILABLE_LEVELS = 10;
	
	private Thread musicThread;
	
    // public Rectangle viewport;
	public static final int VIRTUAL_WIDTH = 800;
    public static final int VIRTUAL_HEIGHT = 480;
    
    public static final float REFERENCE_HEIGHT = 480f;
    
	public InputAdapter backprocessor;
	private TextureAtlas atlas;
	public TextureAtlas gfx_atlas;
	public Skin skin;
	public Skin gfx;
	public OptionsManager optionsManager;
	public SaveManager saveManager;
	public SoundManager soundManager;
	private MusicManager musicManager;
	private DogeCycle dogeCycle;
	
	
	public MayGame (IActivityRequestHandler myRequestHandler) {
		this.myRequestHandler = myRequestHandler;
	}
	
	@Override
	public void create () {
		backprocessor = new InputAdapter() {
			@Override
			public boolean keyDown (int keycode) {
//				System.out.println("Backprocessor, key pressed: " + keycode);
				if (keycode == Keys.F5) {
					getScreen().show();
				}
				return false;
			}
		};
		
		atlas = new TextureAtlas(Gdx.files.internal("data/skin/maygame_default_skin.pack"));
		skin = new Skin(Gdx.files.internal("data/skin/maygame_default_skin.json"), atlas);
		
		gfx_atlas = new TextureAtlas(Gdx.files.internal("data/skin/maygame_gfx.pack"));
		gfx = new Skin(gfx_atlas);
		
		saveManager = new SaveManager();
		optionsManager = new OptionsManager(this);
		soundManager = new SoundManager(this);
		dogeCycle = new DogeCycle();
		
		Gdx.app.addLifecycleListener(dogeCycle);
		
		musicManager = new MusicManager(this);
		
		musicThread = new Thread(musicManager);
		musicThread.setDaemon(true);
		musicThread.start();
		
		Gdx.app.setLogLevel(Application.LOG_NONE);
		
		setScreen(new MainmenuScreen(this));
	}

	@Override
	public void render() {      
        super.render();
	}
	
	@Override
	public void dispose() {
		super.dispose();
		soundManager.dispose();
		gfx_atlas.dispose();
		gfx.dispose();
		atlas.dispose();
		skin.dispose();
		musicManager.dispose();
		musicThread.interrupt();
	}
	
	public static Game getGame() {
		return ((Game) Gdx.app.getApplicationListener());
	}
	
	public MusicManager getMusicManager() {
		return musicManager;
	}
	
	public IActivityRequestHandler getRequestHandler () {
		return myRequestHandler;
	}
	
	public DogeCycle getDogeCycle() {
		return dogeCycle;
	}
}

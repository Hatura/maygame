package de.hatur.maygame.entity;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

import de.hatur.maygame.haturlib.HaturLib;
import de.hatur.maygame.screens.GameScreen;

public class Dolan extends Enemy implements Entity {

	private GameScreen gameScreen;
	private Body body;
	
	private boolean bWantsToEmote = false;
	private Random randomGenerator;
	private int iDolanPlayerEmoteType = -1;
	private int iDolanPlayerEmoteColor = -1;
	
	private final float fDolanSeeRange = 7f;
	private final float fDolanspeed = 5.0f;
	
	public static ArrayList<Dolan> dolanList = new ArrayList<Dolan>();
	
	public static ArrayList<String> dolanSeePlayerEmotes = new ArrayList<String>();
	static {
		dolanSeePlayerEmotes.add("Is that you gooby?");
		dolanSeePlayerEmotes.add("Gooby?");
		dolanSeePlayerEmotes.add("Lemme inspect you");
		dolanSeePlayerEmotes.add("Let's celebrate, doge!");
		dolanSeePlayerEmotes.add("YOU LIK COBIC SANS?");
		dolanSeePlayerEmotes.add("COME TO UNCLE DOLAN");
		dolanSeePlayerEmotes.add("Doge!");
		dolanSeePlayerEmotes.add("Doge my friend");
		dolanSeePlayerEmotes.add("Hey doge, have you seen this?");
		dolanSeePlayerEmotes.add("You look good today doge");
		dolanSeePlayerEmotes.add("Cats pay good, sorry doge");
		dolanSeePlayerEmotes.add("Is that Doge?");
	}
	
	public Dolan(GameScreen gameScreen, Body body) {
		super(body);
		this.gameScreen = gameScreen;
		this.body = body;
		
		this.body.setGravityScale(0);
		
		dolanList.add(this);
		randomGenerator = new Random();
	}
	
	@Override
	public void update() {
		if (HaturLib.isVector2inRangeOfVector2(body.getPosition(), gameScreen.getPlayer().getPlayerBody().getPosition(), fDolanSeeRange)) {
			bWantsToEmote = true;
			float fDolanAngle = (float) Math.atan2(body.getPosition().y - gameScreen.getPlayer().getPlayerBody().getPosition().y, body.getPosition().x - gameScreen.getPlayer().getPlayerBody().getPosition().x);
			body.setTransform(body.getPosition(), fDolanAngle);
			Vector2 vForce = new Vector2(-((float) Math.cos(body.getAngle()) * fDolanspeed), -((float) Math.sin(body.getAngle()) * fDolanspeed));
			if (body.getMass() > 1.0) { // Speed boost big dolans
				vForce.x *= 60;
				vForce.y *= 60;
			}
			body.applyForceToCenter(vForce, true);
		}
		else {
			// Reset font stuff if player is out of range
			body.setAngularVelocity(0);
			bWantsToEmote = false;
			iDolanPlayerEmoteType = -1;
			iDolanPlayerEmoteColor = -1;
		}
	}
	
	public boolean wantsToEmote() {
		return bWantsToEmote;
	}
	
	public void doDolanEmote(SpriteBatch batch, BitmapFont font) {
		if (iDolanPlayerEmoteColor == -1)
			iDolanPlayerEmoteColor = randomGenerator.nextInt(HaturLib.randomColorList.size());
		
		if (iDolanPlayerEmoteType == -1)
			iDolanPlayerEmoteType = randomGenerator.nextInt(dolanSeePlayerEmotes.size());
		
		font.setColor(HaturLib.randomColorList.get(iDolanPlayerEmoteColor));
		font.draw(batch, dolanSeePlayerEmotes.get(iDolanPlayerEmoteType), body.getPosition().x + 0.75f, body.getPosition().y + 0.375f);
	}

}

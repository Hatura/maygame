package de.hatur.maygame.entity;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

import de.hatur.maygame.handles.DogeData;
import de.hatur.maygame.haturlib.HaturLib;

/**
 * Stuff just every enemy has to have!
 * @author Hatura
 *
 */
public class Enemy implements Entity {
	
	private Body body;
	private Random randomGenerator;
	
	private Vector2 lastPosition;
	private float lastAngle;
	
	private int iDoEmote = -1;
	private int iEmoteRandom = -1;
	private int iColorRandom = -1;
	
	// One for all
	private static ArrayList<String> catVictoryEmotes = new ArrayList<String>();
	static {
		catVictoryEmotes.add("Fuck you doge");
		catVictoryEmotes.add("Long live catempire");
		catVictoryEmotes.add("R.I.P. in pepperoni doge");
		catVictoryEmotes.add("WE GOT HIM");
		catVictoryEmotes.add("Moon is ours!");
		catVictoryEmotes.add("All hail monorail cat!");
		catVictoryEmotes.add("You weak doge!");
		catVictoryEmotes.add("That was all doge?");
		catVictoryEmotes.add("Canis mortuus non mordet");
		catVictoryEmotes.add("Catcoin is the future");
		catVictoryEmotes.add("FAK U DOGE");
		catVictoryEmotes.add("Get noscoped!");
		catVictoryEmotes.add("\\�_�/ CATCOIN");
		catVictoryEmotes.add("Pls no dogerino!");
		catVictoryEmotes.add("VICTORY");
		catVictoryEmotes.add("Get back to earth were you belong");
	}
	
	private static ArrayList<String> dolanVictoryEmotes = new ArrayList<String>();
	static {
		dolanVictoryEmotes.add("This is dolan!");
		dolanVictoryEmotes.add("Doge, pls");
		dolanVictoryEmotes.add("Mom get the camera!");
		dolanVictoryEmotes.add("GET REKT DOGE");
		dolanVictoryEmotes.add("Where is my money?");
		dolanVictoryEmotes.add("Mhhh.. doge..");
		dolanVictoryEmotes.add("Too late to turn back");
	}
	
	public Enemy(Body body) {
		this.body = body;
		
		lastPosition = body.getPosition();
		lastAngle = body.getAngle();
		
		randomGenerator = new Random();
	}
	
	@Override
	public void update() {
	}
	
	@Override
	public void victoryEmote(SpriteBatch batch, BitmapFont font) {
		if (iDoEmote == -1)
			iDoEmote = randomGenerator.nextInt(3); // 0-2, 0 = do emote
		if (iDoEmote > 0)
			return;
		
		if (iColorRandom == -1)
			iColorRandom = randomGenerator.nextInt(HaturLib.randomColorList.size());
		
		font.setColor(HaturLib.randomColorList.get(iColorRandom));
		
		DogeData dd = (DogeData) (body.getUserData());
		
		if (dd.getType().equals("groundcat") || dd.getType().equals("flycat")) {
			if (iEmoteRandom == -1)
				iEmoteRandom = randomGenerator.nextInt(catVictoryEmotes.size());
			font.draw(batch, catVictoryEmotes.get(iEmoteRandom), body.getPosition().x + 0.45f, body.getPosition().y + 1.2f);
		}
		else if (dd.getType().equals("dolan")) {
			if (iEmoteRandom == -1)
				iEmoteRandom = randomGenerator.nextInt(dolanVictoryEmotes.size());
			font.draw(batch, dolanVictoryEmotes.get(iEmoteRandom), body.getPosition().x + 0.75f, body.getPosition().y + 0.375f);
		}
	}

	@Override
	public void saveLastPosition() {
		lastPosition = body.getPosition();
		lastAngle = body.getAngle();
	}

}

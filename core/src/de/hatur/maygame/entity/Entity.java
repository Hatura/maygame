package de.hatur.maygame.entity;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * The enemy interface is securing access to the enemy bodies, instaed of filtering or looping them, we can directly access them
 * @author Hatura
 *
 */
public interface Entity {
	public enum EntityDirection {
		LEFT,
		RIGHT,
		TOP,
		BOTTOM
	};
	
	public static ArrayList<Entity> minionList = new ArrayList<Entity>();
	public void update();
	public void saveLastPosition();
	public void victoryEmote(SpriteBatch batch, BitmapFont font);
}

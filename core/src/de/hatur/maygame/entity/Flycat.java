package de.hatur.maygame.entity;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

public class Flycat extends Enemy implements Entity {
	private Body body;
	private EntityDirection direction;
	private boolean bVertical;
	private BitmapFont font;
	
	private Vector2 vLastSafePoint;
	private int iCorrectionCooldown = 750;
	private long lastCorrectionTime;
	
	private final float flycatSpeed = 225.0f;
	
	public Flycat(Body body, EntityDirection startDirection, boolean bVertical) {
		super(body);
		this.body = body;
		this.direction = startDirection;
		this.bVertical = bVertical;
		
		this.body.setGravityScale(0);
		
		vLastSafePoint = body.getPosition();
		lastCorrectionTime = System.currentTimeMillis();
		
		Entity.minionList.add(this);
	}
	
	@Override
	public void update() {
		// TODO: Rewrite with linearimpulse
		if (bVertical) {
			if (direction == EntityDirection.TOP && body.getLinearVelocity().y < 0.5f) {
				body.applyForceToCenter(0, flycatSpeed, true);
			}
			else if (direction == EntityDirection.BOTTOM && body.getLinearVelocity().y > -0.5f) {
				body.applyForceToCenter(0, -flycatSpeed, true);
			}
			
			// correct course if hitting wall
			if (body.getLinearVelocity().y < -0.2f || body.getLinearVelocity().y > 0.2f)
				vLastSafePoint = body.getPosition();
			else if (System.currentTimeMillis() - iCorrectionCooldown > lastCorrectionTime){
				body.setTransform(vLastSafePoint, 0.0f);
				direction = direction == EntityDirection.TOP ? EntityDirection.BOTTOM : EntityDirection.TOP;
				lastCorrectionTime = System.currentTimeMillis();
			}
		}
		else { // Horizontal
			if (direction == EntityDirection.RIGHT && body.getLinearVelocity().x < 0.5f) {
				body.applyForceToCenter(flycatSpeed, 0, true);
			}
			else if (direction == EntityDirection.LEFT && body.getLinearVelocity().x > -0.5f) {
				body.applyForceToCenter(-flycatSpeed, 0, true);
			}
			
			// correct course if hitting wall
			if (body.getLinearVelocity().x < -0.2f || body.getLinearVelocity().x > 0.2f)
				vLastSafePoint = body.getPosition();
			else if (System.currentTimeMillis() - iCorrectionCooldown > lastCorrectionTime){
				body.setTransform(vLastSafePoint, 0.0f);
				direction = direction == EntityDirection.RIGHT ? EntityDirection.LEFT : EntityDirection.RIGHT;
				lastCorrectionTime = System.currentTimeMillis();
			}
		}
	}
}

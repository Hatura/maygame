package de.hatur.maygame.entity;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

public class Groundcat extends Enemy implements Entity {
	
	private Body body;
	private EntityDirection direction;
	
	private Vector2 vLastSafePoint;
	private int iCorrectionCooldown = 750;
	private long lastCorrectionTime;
	
	private final float groundcatSpeed = 150.0f;
	
	public Groundcat(Body body, EntityDirection startDirection) {
		super(body);
		this.body = body;
		this.direction = startDirection;
		
		vLastSafePoint = body.getPosition();
		lastCorrectionTime = System.currentTimeMillis();
		
		Entity.minionList.add(this);
	}
	
	@Override
	public void update() {
		// walk
		if (direction == EntityDirection.RIGHT && body.getLinearVelocity().x < 0.5f) {
			body.applyForceToCenter(groundcatSpeed, 0, true);
		}
		else if (direction == EntityDirection.LEFT && body.getLinearVelocity().x > -0.5f) {
			body.applyForceToCenter(-groundcatSpeed, 0, true);
		}
		
		// correct course if hitting wall or falling
		if (body.getAngle() > -0.05f && body.getAngle() < 0.05f && body.getLinearVelocity().y > -0.05f && (body.getLinearVelocity().x < -0.2f || body.getLinearVelocity().x > 0.2f))
			vLastSafePoint = body.getPosition();
		else if (System.currentTimeMillis() - iCorrectionCooldown > lastCorrectionTime){
			body.setTransform(vLastSafePoint, 0.0f);
			direction = direction == EntityDirection.RIGHT ? EntityDirection.LEFT : EntityDirection.RIGHT;
			lastCorrectionTime = System.currentTimeMillis();
		}
	}
}

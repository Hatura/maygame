package de.hatur.maygame.entity;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.MassData;

import de.hatur.maygame.screens.GameScreen;

public class BuzzSaw implements Entity {
	
	private GameScreen gameScreen;
	private Body body;

	public BuzzSaw(GameScreen gameScreen, Body body) {
		this.gameScreen = gameScreen;
		this.body = body;
		
		// Just update the data since we don't have a mass fetch for objects yet
		this.body.setFixedRotation(true);
		MassData soa = new MassData();
		soa.mass = 20000.0f;
		this.body.setMassData(soa);
		this.body.setGravityScale(0);
		body.setAngularVelocity(10f);
	}
	
	@Override
	public void update() {
		
	}

	@Override
	public void saveLastPosition() {
	}

	@Override
	public void victoryEmote(SpriteBatch batch, BitmapFont font) {
	}

}

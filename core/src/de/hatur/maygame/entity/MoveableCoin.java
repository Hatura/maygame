package de.hatur.maygame.entity;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

import de.hatur.maygame.handles.DogeData;

/**
 * Moving coins need to think aswell to change directions..
 * @author Hatura
 *
 */
public class MoveableCoin implements Entity {
	
	private Body body;
	private EntityDirection direction;
	private boolean bVertical;
	
	private Vector2 lastPosition;
	private float lastAngle;
	
	private boolean bStartInit;
	
	private final float COINSPEED = 7.0f;
	
	public MoveableCoin(Body body, EntityDirection startDirection, boolean bVertical) {
		this.body = body;
		this.direction = startDirection;
		this.bVertical = bVertical;
		
		lastPosition = body.getPosition();
		lastAngle = body.getAngle();
		
		this.body.setGravityScale(0);
		
		bStartInit = false;
	}

	@Override
	public void update() {
		if (bVertical) {
			
		}
		else {
			// Start moving when game has started / entity starts thinking
			if (!bStartInit) {
				if (direction == EntityDirection.LEFT)
					body.setLinearVelocity(-COINSPEED, 0);
				else
					body.setLinearVelocity(COINSPEED, 0);
				bStartInit = true;
			}
			
			DogeData dd = (DogeData) body.getUserData();
			if (dd.isCollided()) {
				if (direction == EntityDirection.LEFT) {
					direction = EntityDirection.RIGHT;
					body.setLinearVelocity(COINSPEED, 0);
				}
				else {
					direction = EntityDirection.LEFT;
					body.setLinearVelocity(-COINSPEED, 0);
				}
				dd.setCollided(false);
			}
		}
	}

	@Override
	public void victoryEmote(SpriteBatch batch, BitmapFont font) {
	}

	@Override
	public void saveLastPosition() {
	}

}

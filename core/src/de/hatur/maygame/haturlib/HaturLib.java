package de.hatur.maygame.haturlib;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

public class HaturLib {
	/**
	 * Used to get a random color (for example font color)
	 */
	public static ArrayList<Color> randomColorList = new ArrayList<Color>();
	static {
		randomColorList.add(new Color(0, 166f/255f, 1f, 1f)); // blue
		randomColorList.add(new Color(157f/255f, 1f, 0, 1f)); // green
		randomColorList.add(Color.YELLOW);
		randomColorList.add(new Color(1f, 187/255f, 0, 1f)); // orange
		randomColorList.add(Color.WHITE);
	}
	
	/**
	 * Checks if a Vector2 is within a given range of another Vector2
	 * @param vec1
	 * @param vec2
	 * @param fRange = Range in meters! 1 Pixel = 1 Meter
	 * @return
	 */
	public static boolean isVector2inRangeOfVector2(Vector2 vec1, Vector2 vec2, float fRange) {
		double dDistance = Math.sqrt(Math.pow((double)(vec2.x - vec1.x), 2) + Math.pow((double)(vec2.y - vec1.y), 2));
		return (double)fRange >= dDistance ? true : false;
	}
	
	public static double getVectorDistance(Vector2 vec1, Vector2 vec2) {
		return Math.sqrt(Math.pow((double)(vec2.x - vec1.x), 2) + Math.pow((double)(vec2.y - vec1.y), 2));
	}
}

package de.hatur.maygame.savemanager;


public class LevelState {
	private int iTotalDogecoin;
	private int iTotalBitcoin;
	private int iTotalLitecoin;
	
	public LevelState(int iLevel, int iTotalDogecoin, int iTotalBitcoin, int iTotalLitecoin) {
		this.iTotalDogecoin = iTotalDogecoin;
		this.iTotalBitcoin = iTotalBitcoin;
		this.iTotalLitecoin = iTotalLitecoin;
	}
	
	public int getTotalDogecoin() {
		return iTotalDogecoin;
	}
	
	public int getTotalBitcoin() {
		return iTotalBitcoin;
	}
	
	public int getTotalLitecoin() {
		return iTotalLitecoin;
	}
}

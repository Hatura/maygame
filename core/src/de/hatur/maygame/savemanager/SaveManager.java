package de.hatur.maygame.savemanager;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.utils.Base64Coder;

import de.hatur.maygame.MayGame;

public class SaveManager {
	
	private final String PREF_NAME = "DOGEQUEST";
	
	private Preferences preferences;
	
	public ArrayList<RecordState> levelRecords;
	public ArrayList<LevelState> levelData;
	
	public SaveManager() {
		preferences = Gdx.app.getPreferences(PREF_NAME);
		
		checkVars();
		loadLevelData();
	}
	
	/**
	 * Check for level data, if not available, create the data
	 */
	private void checkVars() {
		boolean bSomethingChanged = false;
		for (int i = 1; i <= MayGame.AVAILABLE_LEVELS; i++) {
			if (!preferences.contains(Base64Coder.encodeString("level" + i + "done"))) {
				bSomethingChanged = true;
				System.out.println("Level " + i + " changed");
				saveValue("level" + i + "done", "false", false);
				saveValue("level" + i + "score", "0", false);
				saveValue("level" + i + "time", "0", false);
				saveValue("level" + i + "dogecoin", "0", false);
				saveValue("level" + i + "bitcoin", "0", false);
				saveValue("level" + i + "litecoin", "0", false);
			}
		}
		
		if (bSomethingChanged)
			preferences.flush();
	}
	
	/**
	 * load level total data and other level vars, starts with 1 not with 0!
	 */
	public void loadLevelData() {
		levelData = new ArrayList<LevelState>();
		levelData.add(null); // add empty zero value
		for (int i = 1; i <= MayGame.AVAILABLE_LEVELS; i++) {
			TiledMap tiledMap = new TmxMapLoader().load("data/tiledmap/level" + i + ".tmx");
			
			int iAmountDogecoin = 0;
			int iAmountBitcoin = 0;
			int iAmountLitecoin = 0;
			
			// count objects
			for (MapObject object : tiledMap.getLayers().get("Box2D").getObjects()) {
				if (object.getProperties().containsKey("sType")) {
				if (object.getProperties().get("sType").equals("dogecoin") || object.getProperties().get("sType").equals("mdogecoin"))
					iAmountDogecoin++;
				else if (object.getProperties().get("sType").equals("bitcoin") || object.getProperties().get("sType").equals("mbitcoin"))
					iAmountBitcoin++;
				else if (object.getProperties().get("sType").equals("litecoin") || object.getProperties().get("sType").equals("mlitecoin"))
					iAmountLitecoin++;
				}
			}
			
			System.out.println("Dogecoin: " + iAmountDogecoin + ", bitcoin: " + iAmountBitcoin + ", litecoin: " +iAmountLitecoin);
			
			LevelState currentState = new LevelState(i, iAmountDogecoin, iAmountBitcoin, iAmountLitecoin);
			levelData.add(currentState);
			tiledMap.dispose();
		}
	}
	
	/**
	 * load personal record data, starts with 1 not with 0 !
	 */
	public void loadLevelRecords() {
		levelRecords = new ArrayList<RecordState>();
		levelRecords.add(null); // add empty zero value
		for (int i = 1; i <= MayGame.AVAILABLE_LEVELS; i++) {
			boolean bUnlocked;
			if (i == 1)
				bUnlocked = true;
			else
				bUnlocked = readBoolean("level" + (i-1) + "done");
			
			RecordState currentState = new RecordState(bUnlocked, readInt("level" + i + "time"), readInt("level" + i + "score"), 
					readInt("level" + i + "dogecoin"), readInt("level" + i + "bitcoin"), readInt("level" + i + "litecoin"));
			levelRecords.add(currentState);
		}
	}
	
	/**
	 * All saved as String in order to encrypt/decrypt it
	 * @param sKey
	 * @param sValue
	 */
	public void saveValue (String sKey, String sValue, boolean bFlush) {
		preferences.putString(Base64Coder.encodeString(sKey), Base64Coder.encodeString(sValue));
		if (bFlush)
			preferences.flush();
	}
	
	public boolean isKeyExistant(String sKey) {
		if (preferences.contains(Base64Coder.encodeString(sKey)))
			return true;
		else
			return false;
	}
	
	public int readInt(String sKey) {
		try {
			String sValue = Base64Coder.decodeString(preferences.getString(Base64Coder.encodeString(sKey)));
			int iValue = Integer.parseInt(sValue);
			return iValue;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			System.out.println("Error! Value of " + sKey + " is no int!");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			System.out.println("Error when trying to to decode int for key: " + sKey);
		}
		System.out.println("Setting preferences key " + sKey + " to 0");
		preferences.putString(Base64Coder.encodeString(sKey), Base64Coder.encodeString("0")); // Set to zero in case of error to prevent future errors
		preferences.flush();
		return 0;
	}
	
	public long readLong(String sKey) {
		try {
			String sValue = Base64Coder.decodeString(preferences.getString(Base64Coder.encodeString(sKey)));
			long value = Long.parseLong(sValue);
			return value;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			System.out.println("Error! Value of " + sKey + " is no long!");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			System.out.println("Error when trying to to decode long for key: " + sKey);
		}
		System.out.println("Setting preferences key " + sKey + " to 0");
		preferences.putString(Base64Coder.encodeString(sKey), Base64Coder.encodeString("0")); // Set to zero in case of error to prevent future errors
		preferences.flush();
		return 0;
	}
	
	public String readString(String sKey) {
		try {
			String sValue = Base64Coder.decodeString(preferences.getString(Base64Coder.encodeString(sKey)));
			return sValue;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			System.out.println("Error when trying to to decode String for key: " + sKey);
		}
		System.out.println("Setting preferences key " + sKey + " to UNDEFINED");
		preferences.putString(Base64Coder.encodeString(sKey), Base64Coder.encodeString("UNDEFINED")); // Set to zero in case of error to prevent future errors
		preferences.flush();
		return "UNDEFINED";
	}
	
	public float readFloat(String sKey) {
		try {
			String sValue = Base64Coder.decodeString(preferences.getString(Base64Coder.encodeString(sKey)));
			float fValue = Float.parseFloat(sValue);
			return fValue;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			System.out.println("Error! Value of " + sKey + " is no float!");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			System.out.println("Error when trying to to decode float for key: " + sKey);
		}
		System.out.println("Setting preferences key " + sKey + " to 0");
		preferences.putString(Base64Coder.encodeString(sKey), Base64Coder.encodeString("0")); // Set to zero in case of error to prevent future errors
		preferences.flush();
		return 0;
	}
	
	public boolean readBoolean(String sKey) {
		try {
			String sValue = Base64Coder.decodeString(preferences.getString(Base64Coder.encodeString(sKey)));
			boolean bValue = Boolean.parseBoolean(sValue);
			return bValue;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			System.out.println("Error! Value of " + sKey + " is no boolean!");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			System.out.println("Error when trying to to decode boolean for key: " + sKey);
		}
		System.out.println("Setting preferences key " + sKey + " to false");
		preferences.putString(Base64Coder.encodeString(sKey), Base64Coder.encodeString("false")); // Set to zero in case of error to prevent future errors
		preferences.flush();
		return false;
	}
}
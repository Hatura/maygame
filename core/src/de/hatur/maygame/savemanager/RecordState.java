package de.hatur.maygame.savemanager;

import java.text.SimpleDateFormat;
import java.util.Date;


public class RecordState {
	
	private boolean bUnlocked;
	private int iTime;
	private int iScore;
	private int iDogecoin;
	private int iBitcoin;
	private int iLitecoin;
	
	public RecordState(boolean bUnlocked, int iTime, int iScore, int iDogecoin, int iBitcoin, int iLitecoin) {
		this.bUnlocked = bUnlocked;
		this.iTime = iTime;
		this.iScore = iScore;
		this.iDogecoin = iDogecoin;
		this.iBitcoin = iBitcoin;
		this.iLitecoin = iLitecoin;
	}
	
	/**
	 * is level unlocked
	 * @return
	 */
	public boolean isUnlocked() {
		return bUnlocked;
	}
	
	/**
	 * get record time in MS
	 * @return time in seconds
	 */
	public int getTime() {
		return iTime;
	}
	
	/**
	 * get record time formatted
	 */
	public String getFormattedTime() {
		SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");
		Date dateNow = new Date(iTime);
		return sdf.format(dateNow);
	}
	
	/**
	 * level highscore
	 * @return level highscore
	 */
	public int getScore() {
		return iScore;
	}
	
	/**
	 * collected dogecoin
	 * @return collected dogecoin
	 */
	public int getDogecoin() {
		return iDogecoin;
	}
	
	/**
	 * collected bitcoin
	 * @return collected bitcoin
	 */
	public int getBitcoin() {
		return iBitcoin;
	}
	
	/**
	 * collected litecoin
	 * @return collected litecoin
	 */
	public int getLitecoin() {
		return iLitecoin;
	}
	
	public double getPercentageDone(LevelState levelState) {
		// bitcoin worth 2
		// litecoin worth 2
		// dogecoin worth 1
		double dPercentage = 
				(((double)iDogecoin + (2 * (double)iBitcoin) + (2 * (double)iLitecoin))
				* 100)
				/ ((double)levelState.getTotalDogecoin() + (2 * (double)levelState.getTotalBitcoin()) + (2 * (double)levelState.getTotalLitecoin()));
		return (Math.round(dPercentage * 100.0) / 100.0);
	}
	
	public void updateRecords(int iTime, int iScore, int iDogecoin, int iBitcoin, int iLitecoin) {
		this.iTime = iTime;
		this.iScore = iScore;
		this.iDogecoin = iDogecoin;
		this.iBitcoin = iBitcoin;
		this.iLitecoin = iLitecoin;
	}
	
	public void setUnlocked() {
		this.bUnlocked = true;
	}
}

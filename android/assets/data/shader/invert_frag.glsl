#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_color;
varying vec2 v_texCoord0;

uniform sampler2D u_texture;

void main() {
	vec4 color = v_color * texture2D(u_texture, v_texCoord0);
	color.rgb = 1.0 - color;
	gl_FragColor = color;
}
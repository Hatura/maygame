#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_color;
varying vec2 v_texCoord0;

uniform vec2 u_resolution;
uniform sampler2D u_texture;
uniform float u_sepiaintensity;
uniform float u_flashintensity;

const vec3 SEPIA = vec3(1.2, 1.0, 0.8);
const vec3 FLASH = vec3(1.0, 0.0, 0.0);

void main() {
	vec4 color = v_color * texture2D(u_texture, v_texCoord0);
	
	// Vignette
	vec2 relativePosition = gl_FragCoord.xy / u_resolution - 0.5;
	float len = length(relativePosition);
	float vignette = smoothstep(0.8, 0.5, len);
	color.rgb = mix(color.rgb, color.rgb * vignette, 0.75);
	
	// Hurt flash
	color.rgb = mix(color.rgb, FLASH, u_flashintensity);
	
	// For sepia fade
	float gray = dot(color.rgb, vec3(0.299, 0.587, 0.114));
	vec3 sepiaColor = vec3(gray) * SEPIA;
	
	color.rgb = mix(color.rgb, sepiaColor, u_sepiaintensity);
	
	gl_FragColor = color * v_color;
}
